# PDF2Blocs

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6605450.svg)](https://doi.org/10.5281/zenodo.6605450)

This project has been moved to [https://forgemia.inra.fr/bsv/pdf2blocs](https://forgemia.inra.fr/bsv/pdf2blocs).

