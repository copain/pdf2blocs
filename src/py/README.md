# Version python de pdf2blocks

## 1 Introduction

### 1.1 Objectif

L'objectif de ce programme est de reconstruire
la structure logique (titre, sous-titre, paragraphe, ...) d'un document pdf.
Pour ce faire, le programme utilise des outils d'extraction du texte contenu
dans des fichiers pdf.

Ce programme est écrit en python (version 3). Il s’intitule pdf2blocks.py.

Il prend en argument un fichier pdf contenant du texte,
des tableaux et des images.

### 1.2 Utilisation

On l'exécute depuis la ligne de commande :

>     python pdf2blocks.py /chemin/vers/fichier.pdf

Le résultat est écrit sur la sortie standard. Il est facile de la rediriger
dans un fichier html.

### 1.3 Exemple de résultat :


```html
(…)
<p class="paragraph" blk="62">En parallèle, 5 parcelles signalent 
  en bordure la présence de cécidomyies dans les siliques (de 2 à 10%) 
  à RENEVE (21), BRETIGNY (21), BEIRE-LE-CHATEL (21) 
  ST BENIN DES BOIS (58) et GY-L'EVEQUE<br />(89). Les piqures de 
  charançon des siliques constituent une porte d'entrée pour les 
  cécidomyies.</p>
<footer class="bottom page" blk="63">3<br /></footer>
<hr /><!-- =============== Page 4 =============== -->
<header class="page top" blk="64">Grandes cultures n° 27 du 21 mai 2019<br /></header>
<h4 class="title 4" blk="65">Analyse du risque :</h4>
<p class="paragraph" blk="66">La majorité des parcelles approche
  de la fin de la période sensible. Le risque est faible.</p>
<h2 class="title 2" blk="67">Pucerons cendrés</h2>
<p class="paragraph" blk="68">30 parcelles observées.</p>
<p class="paragraph" blk="69">Description du ravageur, période de risque
  et seuil de nuisibilité : reportez-vous au BSV n°24 du 30 avril 2019.</p>
<h4 class="title 4" blk="70">Observations :</h4>
<p class="paragraph" blk="71">La situation n'a pas évolué depuis
  la semaine dernière. On signale la présence de ce ravageur en bordure 
  À SAINT-LOTHAIN (39), SAINT-BENIN-DES-BOIS (58)
  et BAIGNEUX LES JUIFS (21), à chaque fois en dessous du seuil
  de nuisibilité.</p>
(…)
```

### 1.4 Dépendances

*pdf2blocks* utilise actuellement *pdftotext* et *pdftohtml*, deux outils basés
sur la librairie poppler, dérivée de Xpdf. Ces deux outils prennent en entrée
un fichier pdf.

*pdf2blocs* utilise aussi la librairie
[PyEnchant](https://pyenchant.github.io/pyenchant/index.html) qui lui fournit
un dictionnaire et des outils de recherche. PyEnchant est distribué sous
la licence [LGPL](http://www.gnu.org/copyleft/lesser.html).

Enfin, pour la reconnaissance de tableaux, *pdf2blocks* utilise 
[camelot-py](https://pypi.org/project/camelot-py/)
([documentation](https://camelot-py.readthedocs.io/en/master/index.html)).
Camelot peut s'appuyer sur *ghostscript* ou *poppler*. Nous avons bien sûr
choisi le second, qui nécessite [pdftopng](https://pypi.org/project/pdftopng/).

*pdftopng* est sous licence
[GPLv2](https://raw.githubusercontent.com/vinayak-mehta/pdftopng/master/LICENSE) 
et *camelot* est sous licence
[MIT](https://github.com/camelot-dev/camelot/blob/master/LICENSE).

#### 1.4.1 pdftotext
*pdftotext* est destiné à produire une sortie en texte brut,
lisible dans une console texte par exemple.

Il peut toutefois écrire ses résultats dans un format xml, avec la structure
suivante :

    <doc>
    <page width="595.320000" height="841.920000">
     <flow>
       <block xMin="419.230000" yMin="218.420000" xMax="544.468000" yMax="230.420000">
         <line xMin="419.230000" yMin="218.420000" xMax="544.468000" yMax="230.420000">
           <word xMin="419.230000" yMin="218.420000" xMax="438.022000" yMax="230.420000">BSV</word>
           <word xMin="440.806000" yMin="218.420000" xMax="451.222000" yMax="230.420000">n°</word>
           <word xMin="453.874000" yMin="218.420000" xMax="466.042000" yMax="230.420000">15</word>
           <word xMin="468.820000" yMin="218.420000" xMax="472.492000" yMax="230.420000">-</word>
           <word xMin="475.180000" yMin="218.420000" xMax="487.348000" yMax="230.420000">25</word>
           <word xMin="490.156000" yMin="218.420000" xMax="517.720000" yMax="230.420000">juillet</word>
           <word xMin="520.264000" yMin="218.420000" xMax="544.468000" yMax="230.420000">2018</word>
         </line>
       </block>
     </flow>
     <flow>
       <block xMin="57.720000" yMin="269.000000" xMax="214.181760" yMax="283.040000">
         <line xMin="57.720000" yMin="269.000000" xMax="214.181760" yMax="283.040000">
           <word xMin="57.720000" yMin="269.000000" xMax="66.228240" yMax="283.040000">A</word>
           <word xMin="69.401280" yMin="269.000000" xMax="118.864200" yMax="283.040000">RETENIR</word>
           <word xMin="121.967040" yMin="269.000000" xMax="157.109160" yMax="283.040000">CETTE</word>
           <word xMin="160.226040" yMin="269.000000" xMax="214.181760" yMax="283.040000">SEMAINE</word>
         </line>
       </block>
            (…)

Les balises *block*, *line* et *word* contiennent des informations de
position, à savoir les coordonnées absolues du plus petit rectangle qui
contient l'élément désigné. Ces coordonnées sont exprimées à l'aide de
quatre attributs : *xMin*, *yMin*, *xMax*, *yMax*, qui semblent exprimés
en pixels. En particulier, pour un mot, `yMax - yMin` correspond à la taille
de la police de caractères exprimée en pixels (*px*).

L'outil *pdftotext* est particulièrement efficace pour identifier les longs
paragraphes contenant plusieurs lignes. En revanche, il ne permet pas
d'extraire correctement  la hiérarchie des titres et sous-titres.
La sortie de l’outil ne donne pas suffisamment d'informations sur les
polices de caractères : le nom de la police de caractères,
les styles (gras, italique, ...) et les couleurs du texte ne sont pas
décrits.

#### 1.4.2 pdftohtml
*pdftohtml* est un outil qui vise à produire une page html qui ressemble
au document pdf. Pour cela, il n'a pas besoin de respecter un ordre
de lecture, car les éléments d'une page html peuvent être localisés.
En revanche, il contient une description précise des polices de caractères
utilisées dans le document.

Plutôt qu'une page html, l'outil peut écrire ses résultats au format xml.

Voici un exemple de sortie xml de l'outil *pdftohtml* :

     <pdf2xml producer="poppler" version="0.80.0">
     <page number="1" position="absolute" top="0" left="0" height="1262" width="892">
     	<fontspec id="0" size="14" family="ABCDEE+Calibri" color="#000000"/>
     	<fontspec id="1" size="10" family="ABCDEE+Verdana" color="#000000"/>
     	<fontspec id="2" size="19" family="ABCDEE+Calibri,Bold" color="#000000"/>
     	<fontspec id="3" size="16" family="ABCDEE+Calibri,Bold" color="#000000"/>
     <text top="1216" left="76" width="16" height="17" font="0">1  </text>
     <text top="1216" left="755" width="67" height="15" font="1">BSV n° 15 </text>
     <text top="403" left="87" width="239" height="21" font="2"><b>A RETENIR CETTE SEMAINE </b></text>
     <text top="473" left="87" width="54" height="18" font="3"><b>Météo </b></text>

On remarque que le texte est découpé en segments de texte,
c'est à dire en parties d'une ligne ayant la même fonte.
Les segments de texte sont identifiés par la balise <text>.
Par exemple,  la ligne

> Le 2<sup>nd</sup> grand évènement,

sera découpé en trois segements de texte :
- Le 2
- nd
- grand évènement,

La ligne suivante est aussi découpée en trois segements de texte

> Le ***petit*** papillon.

car un nouveau style (gras, italique) de la même police est défini comme une
nouvelle fonte. On note que dans *pdftohtml* les lignes ne sont pas
identifiées (on ne sait pas à priori quels segments appartiennent à une
même ligne).
On peut toutefois reconstruire une ligne en se basant sur
les valeurs identiques de l'attribut *top* d'un ensemble de segments de texte.
Attention, il se peut que les valeurs de l'attribut top  ne soient  pas
toujours égales dans une même ligne, notamment quand la ligne contient des
indices ou des exposants.

Enfin, *pdftohtml* a du mal à identifier les colonnes.
Par exemple, deux lignes situées à la même hauteur mais dans deux colonnes
successives sont parfois regroupées dans le même segment de texte.

À noter que la version 22-06 des librairies poppler a eu des conséquences
sur la détermination des tailles de police de caractères, qui ont eu
des incidences sur les sorties de *pdf2blocks*. Pour pallier cet inconvénient,
l'algorithme a été adapté pour utiliser les hauteurs de lignes plutôt
que les tailles de polices de caractères.

## 2 L'algorithme de pdf2blocks

*pdf2bocks* fusionne les blocs issus de *pdftotext* avec les fontes de
*pdftohtml* pour dériver une liste ordonnée de blocs caractérisés par des
éléments de la structure logique.


### 2.1 pdftotext
Le programme *pdf2blocks* commence par lancer la commande suivante :

     pdftotext -bbox-layout -eol unix /path/to/file.pdf

Le résultat de cette commande est stocké dans une liste python. Cette liste
a été nommée ***blocks***. Cette liste est initialisée avec une partie des
sorties de *pdftotext*.

Cette liste reprend la structure xml de *pdftotext* à partir de la balise
*block*.
Les balises *page* de pdftotext ont été remplacées par un attribut associé
aux éléments de la liste *blocks* donnant le numéro de page.
Les balises *flow* de *pdftotext* ont aussi été remplacées par un attribut
pour les éléments cette liste. La valeur de cet attribut est  incrémenté à
chaque fois que la balise *flow* est rencontrée.
On peut ainsi identifier les blocs qui font partie d'un même flow.

Les éléments de la liste *blocks* ont donc la structure suivante :
 *blocks*:
  - **page :** Le numéro de page calculé à partir de la sortie de pdftotext.
  - **flow :** Un identifiant unique pour chaque balise flow calculé à partir
  de la sortie de pdftotext.
  -  **x_min**, **x_max**, **y_min** et **y_max** : Les coordonnées du bloc
  dans la page, données par pdftotext.
  -  **h_min** et **h_max** : Les hauteurs minimum et maximum des lignes du
  bloc (calculées à partir des valeurs *height* de chaque ligne du bloc,
  voir ci-dessous).
  - **right**, **left**, **top**, **down** : La distance au bloc le plus
  proche situé respectivement à droite, à gauche, au-dessus et en-dessous
  du présent bloc. Cette distance est calculée à l'aide des xMin, xMax, ...
  Elle est négative s'il n'y a pas de bloc à la droite (à la gauche, ...)
  du bloc.
  - **right_block**, **left_block**, ... : Désignent le bloc le plus proche
  à la droite, (à gauche, ....) du bloc. Il s'agit du bloc retenu comme
  étant la plus proche lors du calcul des distances right, left, ...
  - **nb_cars** et **nb_words** : Le nombre de caractères et le nombre de
  mots du bloc (calculés).
  - **class :** contient le résultat du processus de classification de blocs.
  La valeur par défaut de cet attribut est BL_UNDEF.
  - **lines :** Une liste, contenant les lignes ordonnées par pdftotext.
  C'est, comme *blocks*, une liste de dictionnaires python, contenant :
     - **text :** Le texte contenu dans cette ligne.  
       Ce texte est la concaténation du contenu des balises word de
      *pdftotext*.
       Il est composé de chaque mot de la ligne séparé d'une espace, sauf si
       le premier mot est un unique caractère plus grand que les suivants
       (dans ce cas on considère que c'est un effet de texte
       et l'espace n'est pas ajoutée).
     - **height :** La hauteur de ligne calculée correspondant
       à ```yMax - yMin```, coordonnées de la ligne,
       renvoyées par *pdftotext*.
     - **nb_words**, **nb_cars** et **flags** : sont les mêmes que pour les
       blocs, avec les informations relatives à la ligne.
     - **words :** Une autre liste de dictionnaires, qui contient :
          - **text :** le mot contenu dans la balise word de *pdftotext*.
          - **height :** la hauteur du mot  calculée correspondant
            à ```yMax - yMin```, coordonnées du mot
            renvoyées par *pdftotext*.

Lors de la concaténation du contenu des balises word effectuée pour renseigner
la valeur de lines['text'], un traitement particulier est effectué pour détecter
et traiter deux cas :

1. Les "L ignes" ayant un premier caractère mis en exergue, qui se retrouve
  séparé du reste du mot. Dans le cas où la première
  ligne d'un bloc commence par une balise *word* d'une seule lettre majuscule,
  on teste si cette lettre est dans le dictionnaire puis si le mot suivant
  l'est aussi. Dans le cas contraire on teste si la concaténation des deux mots
  est dans le dictionnaire. Si c'est le cas, alors on garde le mot concaténé.
  À noter que pour les aulx, 'A' et 'il' sont dans le dictionnaire. Néanmoins,
  l'absence de sens grammatical d'une phrase commençant par "A il " nous
  permet de le traiter comme un cas particulier.
1. Les "T I T R E S" ou lignes mal justifiées dans lesquelles les espacements
  de caractères sont tels que *pdftotext* insère une espace entre chaque
  caractère. Ces lignes sont détectées en calculant la moyenne du nombre de
  lettres par mot. En-dessous d'un seuil (actuellement 3), la proportion de
  lettres et d'espaces est calculée et doit être supérieure à un seuil
  (actuellement deux-tiers).
  Pour les lignes qui correspondent, le traitement repose sur la recherche de
  deux valeurs d'espacement horizontal, pour tenter de distinguer
  un espacement entre deux lettres d'un espacement entre deux mots. À l'aide
  de ces valeurs, la phrase est recomposée en collant les lettres. Ensuite,
  un comptage des mots présents dans le dictionnaire est effectué sur la ligne
  ainsi recomposée et est comparé au même comptage effectué sur le ligne
  renvoyée par *pdftotext*. Le texte ayant la plus forte proportion de mots
  trouvés dans le dictionnaire est conservé.

### 2.2 pdftohtml
Ensuite, le programme *pdf2blocks* lance la commande *pdftohtml* :

     pdftohtml -xml -i -stdout /path/to/file.pdf

#### 2.2.1 Les fontes renvoyées par *pdftohtml*
Les fontes de caractères renvoyées par *pdftohtml* sont stockées dans une
liste python nommée **fontspec** (du nom de la balise xml associée).
Les éléments de cette liste sont des dictionnaires python ayant la structure
suivante :

- **id :** Un identifiant unique pour désigner la fonte.
- **size :** La taille de la fonte, en "pixels" (px).
- **family :** Le nom de la fonte, tel que renvoyé par *pdftohtml*,
  éventuellement suivi d'attributs de style séparés par des virgules.
  Exemples :
    - "ABCDEE+Calibri"
    - "ABCDEE+Calibri,Bold"
    - "ABCDEE+Calibri,BoldItalic"
    - "ABCDEE+Symbol"
- **color :** La couleur du texte, en format html. Exemples :
    - "#000000"
    - "#b366b3"
- **nb_cars** nombre de caractères qui ont cette fonte. Ce nombre n'est pas
  présent dans le résultat xml de la commande *pdftohtml*. Il est calculé
  lors de l'extraction du texte.

#### 2.2.2 Le texte renvoyé par la commande *pdftohtml*
Les segments de texte renvoyés par *pdftohtml* représentent la plupart du
temps une ligne de texte entière. La liste qui les contient a été nommée
**segments**. Ses éléments, extraits du résultat xml de *pdftohtml*,
ont la structure suivante :

- **text :** Le texte du segment, contenu de la balise \<text\> de pdftohtml.
- **font :** L'identifiant de la fonte pour ce segment.
- **page :** Le numéro de page du segment.
- **top**, **left**, **width** et **height** : Les coordonnées du segment de
  texte dans la page.

### 2.3 Détection des tableaux

La version précédente *pdf2blocks* avait des difficultés à reconnaître
les tableaux. Or il existe plusieurs librairies dont la reconnaissance
de tableaux (et l'extraction de données) des fichiers pdf est
l'objectif.

Parmi celles-ci nous avons choisi *camelot*, qui peut être appelée en 
python, est très configurable et est actuellement maintenue. *Camelot*
peut de plus s'appuyer sur *poppler*, ce qui est dans la continuité
des dépendances de *pdf2blocks*.

*Camelot* est appelé avec les options par défaut. Celles-ci recherchent
les tableaux en détectant des lignes horizontales et verticales
composant des bordures.

Les options suivantes ont été testées mais n'ont pas été utilisées :
- l'option **split-text=True**, qui recherche les cellules multi-lignes,
interfère et donne de très mauvais résultats avec les bulletins
multi-colonnes,
- l'option **flavor='stream'** renvoie trop de faux positifs,
- enfin, le très faible nombre de tableaux basés sur une atternance
de couleurs de fond rend l'option **process_background=True**
peu utilisable.

Les options choisies laissent passer nombre de faux-négatifs mais
de fait interfère peu en dehors des tableaux.

*Camelot* présente l'avantage de renvoyer des valeurs qualitatives
associées aux résultats, ce qui permet de les sélectionner. 
Ainsi, seuls les tableaux ayant une valeur **accuracy** supérieure*
à 90.0 et une valeur **whitespace** inférieure à 50.0 sont retenus.
Les tableaux n'ayant qu'une seule colonne sont ignorés, sauf dans le
cas où un caractère de séparation est détecté dans chaque ligne ;
ce cas correspond aux tableaux n'ayant pas de bordures verticales.

### 2.4 Traitements
À ce point, nous avons trois listes, **blocks**,
**fontspec** et **segments**, dont la structure est décrite ci-dessus.

La plupart des traitements sont effectués sur la liste **blocks**, qui
contient une base de la structure logique du document. Le but est
d'identifier le rôle de chacun des blocs (titre, sous-titre, ...).

La liste **blocks** contient le résultat de la commande *pdftotext*, dont le
but est de restituer, à partir du document pdf, un texte lisible de haut en bas
dans une console texte.
Toutefois, l'ordonnancement des blocs par *pdftotext* s'est révélé peu
adapté à la lecture des Bulletins de Santé du Végétal. C'est pourquoi
un ré-ordonnancement des blocs spécifique est effectué par *pdf2blocks*.

Les chapitres suivants décrivent les traitements, effectués séquentiellement,
sur les données issues des deux commandes *pdftotext* et *pdftohtml*.

#### 2.4.1 Marquage des tableaux
Les blocs contenus dans les tableaux détectés par *camelot*
(identifiés grâce à leurs coordonnées) sont remplacés par un unique
bloc marqué comme étant un tableau. Il n'y aura pas d'autre détection
d'élément de structure sur ces blocs (puisqu'ils sont déjà identifiés
comme tableaux), et ils sont ignorés du reste des traitements à
l'exception de l'ordonnancement des blocs.

#### 2.4.2 Détermination de la taille de la fonte par défaut
La taille de fonte par défaut est déterminée à partir de la liste **segments**
et **fontspec**, c'est à dire à partir des sorties de *pdftohtml*.

La taille retenue est celle du plus grand nombre de caractères associés à
chaque taille de fonte de la liste **fontspec**.

#### 2.4.3 Détection des pieds de page

Les "pieds de page" d’un document sont une zone de texte répétée
à chaque fin de page contenant par exemple le numéro de la page
et un texte caractéristique du document, comme son titre ou ses auteurs.

Les notes de bas de page ne sont pas considérées comme faisant partie
des pied de page.

L'algorithme utilise les listes *lines* contenues dans **blocks**.
Il consiste à tester si la dernière ligne de chaque page
a le même contenu textuel, en ne considérant que les caractères alphabétiques
([a-zA-Z]). Les caractères numériques ne sont pas pris en compte afin que
les numéros de pages puissent être considérés comme des pieds de page.

Tant que des lignes sont détectées comme étant des pieds de page,
on teste la ligne précédente, et ainsi de suite.

Les lignes détectées comme étant des bas de page ont la valeur
BL_BOTTOM_PAGE dans leur attribut class.

#### 2.4.4 Détection des en-tête

L'algorithme est similaire à la détection des pieds de page, mais au lieu
de s'appliquer aux dernières lignes de chaque page, il considère les premières
lignes.

D'autre part, si le document n'a que deux pages, on regarde si les premières
lignes de chacune des deux pages sont identiques pour détecter les en-têtes.  
Si le document a plus de deux pages, on détecte d'abord s'il existe des
en-têtes à partir de la seconde page et ensuite on teste si il existe
une en-tête avec le même contenu  dans la première page.

Les lignes détectées comme étant des en-tête ont la valeur BL_TOP_PAGE
dans leur attribut class.


#### 2.4.5 Attribution de fontes aux blocs
Il s'agit d'attribuer l'*id* d'une fonte de **fontspec** à chaque ligne
de la liste **blocks**.

L'algorithme consiste, pour chaque ligne de bloc (élément de la liste
**lines**), à calculer sa
[distance de Levenshtein](https://en.wikipedia.org/wiki/Levenshtein_distance)
avec chaque segment de texte (élément de la liste **segments**). Ce calcul
ne se fait qu'entre blocs et segments ayant une intersection (non vide).
On attribue alors à la ligne la fonte du segment ayant le meilleur score
(c'est à dire la distance la plus faible).

Ce calcul de distance est nécessaire du fait de la segmentation des lignes
renvoyées par *pdftohtml*, qui ne correspondent pas exactement aux lignes
renvoyées par *pdftotext*. Par ailleurs, l'ordre des lignes dans une même
page est parfois différent entre les deux outils.

Lorsqu'une ligne est reconnue à l'identique, elle est marquée et
n'est plus utilisée dans l'algorithme.

L'implémentation du calcul de l'intersection entre blocs et segments
a été ajoutée récemment (les sorties de *pdftohtml* n'utilisent pas les
mêmes grandeurs que celles de *pdftotext*), et une étude plus poussée
sur les conditions d'intersection serait à mener et pourrait conduire
à s'affranchir de la distance de Levenstein et à une optimisation
de cet algorithme.

#### 2.4.6 Ré-ordonnancement des blocs
Avant de reconstituer la structure du document, un ré-ordonnancement des blocs
est effectué, en deux temps :

1. détection de colonnes
1. parcours de l'ensemble des blocs et tenant compte des positions des colonnes
  suivant le sens de lecture.

Ceci est effectué page par page.

##### 2.4.6.1 Détection de colonnes
La détection de colonnes ne considère que les blocs :

- qui ne sont pas marqués comme tableaux,
- de plus de 3 lignes
  (ce seuil pouvant être modifié dans la constante MIN_LINES_IN_COLUMN_BLOCK),
- dont la taille de fonte est inférieure ou égale à la taille de fonte
  par défaut,
- et dont la longueur de ligne est au moins égal à un seuil
  (MIN_CAR_IN_COLUMN_BLOCK - actuellement 20), pour les distinguer des colonnes
  de tableaux qui n'auraient pas été détextés par *camelot*.

Sur ces blocs :

1. On cherche le bloc situé le plus à gauche de la page (le plus petit x_min).
1. On place une verticale V1 à gauche de ce bloc.
   C'est la verticale de la première colonne.
3. Puis on parcourt l'ensemble des blocs dont l'arête gauche (x_min) est située
   à proximité de V1 pour chercher le bloc le moins large (le plus petit x_max).
4. On pose une seconde verticale V' sur l'arête droite de ce bloc (x_max).
5. On parcourt l'ensemble des blocs, pour construire l'ensemble des blocs dont
   l'arête de gauche (x_min) est à droite de V'.
6. On détermine le bloc le plus à gauche de cet ensemble (le plus petit x_min) .
7. On place une verticale intitulée V2 sur l'arête gauche de ce bloc (x_min).
   Il s'agit de la verticale de la seconde colonne.

On recommence le processus de détection d'une autre colonne depuis le point 3
(en considérant Vi=V2, puis V3, ...), tant qu'il y a des blocs à l'étape 5.
Toutefois, on considère l'ensmble des blocs dont l'arête gauche est
à proximité de Vi ou à gauche de Vi pour la détermination de V'.

La dernière verticale est placée au niveau de l'arête droite
du bloc situé le plus à droite (le plus grand x_max).

Les verticales ainsi définies seront ensuite utilisées pour identifier des
colonnes. Une colonne est localisée entre deux verticales.

L'ensemble des éléments de la liste *blocks* est enrichi avec deux nouveaux
attributs :

- **col_min** : le numéro de colonne contenant l'arête gauche du bloc,
- **col_max** : le numéro de colonne contenant l'arête droite du bloc.

En effet, certains blocs peuvent chevaucher plusieurs colonnes (par exemple
des titres). Les deux attributs ont la même valeur quand le bloc
est entièrement contenu dans une seule colonne.

Hypothèse de travail :  
Si une page contient plusieurs colonnes et qu'un titre chevauche plusieurs
colonnes, le sens de lecture sera de lire en premier tous les blocs au dessus
de ce titre sur toutes les colonnes.  
Si une page contient plusieurs colonnes et que les titres sont totalement
contenus dans une colonne, le sens de lecture se fera colonne par colonne.

Pour définir le sens de lecture on effectue l'opération suivante :
les blocs ayant une fonte plus grande que la fonte par défaut (les titres
potentiels) sont élargis autant que possible vers la droite de la page
(accroissement de leur x_max) jusqu'à rencontrer un autre bloc
ou jusqu'au bord de la page. La valeur de *col_max* est ajustée en conséquence.

![figure A](images/Titles.gif)  

Cette opération est nécessaire pour définir le sens de lecture correct
quand un titre pourrait déborder sur plusieurs colonnes mais qu'il est
trop court, et de fait entièrement contenu dans une seule colonne.


##### 2.4.6.2 Parcours des blocs dans le sens de lecture

L'objectif est d'ordonner les blocs d'une même page dans leur sens de lecture,
c'est à dire de haut en bas et de gauche à droite.

Une nouvelle liste ordonnée **ordered_blocks** de blocs est créée
pour une page donnée.

Les en-têtes sont tout d'abord insérées dans la liste **ordered_blocks**
dans l'ordre donné par *pdftotext*.

Au fur et à mesure que les blocs sont ajoutés dans la liste **ordered_blocks**,
ils sont retirés des blocs à parcourir (stockés dans la liste **blocks**).
L'algorithme effectue une boucle jusqu'à épuisement des blocs à parcourir.

On définit une zone rectangulaire à traiter inclue dans la page. Cette zone est
initialisée à la première colonne (la colonne la plus à gauche).

La boucle principale de l'algorithme effectue les tâches suivantes :

- Recherche du bloc **H** le plus haut dont l'arête gauche est dans la zone ;
  Ce bloc va définir l'arête haute de la zone.
  - si aucun bloc n'est trouvé cela veut dire qu'il n'existe pas de bloc
    dont l'arête gauche se trouve dans la zone. Alors, la zone est élargie
    vers la droite et une nouvelle recherche est relancée.
  - si aucun bloc n'est trouvé dans la zone
    et que la zone comprend la dernière colonne,
    on élargit la zone (à gauche) à l'ensemble des colonnes.  
    Ceci est dû aux déplacements de la zone dans le processus de parcours,
    qui peut avoir décalé la zone vers la droite en laissant des blocs
    à gauche.
  - Le cas où l'on ne trouve pas de bloc et que la zone comprend l'ensemble
    des colonnes signifie la fin de l'algorithme.
    Il n'y a plus de blocs à traiter.
- Parmi tous les blocs dont l'arête haute est au même niveau que l'arête haute
  de **H** (modulo un petit intervalle correspondant à la valeur de
  VERTICAL_ALIGMENT_THRESHOLD), on cherche le bloc le plus à gauche intitulé
  'HG' (le bloc le plus haut et le plus à gauche de la zone).
  **HG** peut être le même bloc que **H**.
- Étant donné **HG**, trois cas sont considérés :
  * **A** : Dans le général, à l'exception des cas **B** et **C**,
    **HG** est ajouté à **ordered_blocks** et la zone est ajustée
    à celle de **HG** (pour que les blocs situés dans la même colonne que **HG**
    soient parcourus avant ceux, éventuellement plus hauts, de la colonne
    suivante).
  - **B** : Si l'arête droite du bloc **HG** est au delà de l'arête droite de
    la zone. C'est par exemple le cas d'un titre sur toute la largeur de
    la page. Dans ce cas, la zone est agrandie jusqu'à l'arête droite de **HG**.
    Tous les blocs de la zone situé au dessus de **HG** sont insérés
    dans la liste **ordered_blocks**, avant **HG**.  
    ![figure B](images/CaseB.gif)  
    *Dans la figure, le bloc "B" doit être lu avant "T".*

  - **C** : Si il existe un bloc non traité dans une colonne à gauche
    de la zone à traiter, et dont le haut est au-dessus du bas de **HG**
    (ce bloc est "au niveau de **HG**").
    Ceci se produit quand un titre sur plusieurs colonnes
    ou un bloc situé à droite (à côté d'une photo par exemple) vient d'être
    ajouté à la liste **ordered_blocks**.  
    Dans ce cas, on réinitialise la zone à l'ensemble des
    colonnes (ce qui aura pour effet de traiter en priorité les blocs
    de gauche).
    ![figure C](images/CaseC.gif)  
    *Sur cette figure, le bloc "C" doit être lu avant "D".*

Une fois cette boucle terminée, **ordered_blocks** contient la liste des blocs
dans un ordre de lecture estimé convenable.

La liste **ordered_blocks** est enrichie des blocs marqués comme pieds de page.

#### 2.4.7 Reconstitution de la structure du document

Les en-tête et pieds de page ont déjà été identifiés,
ainsi que les tableaux.

Les lignes ne comportant aucun caractère alphanumérique sont ignorées.

L'algorithme de reconstitution de la structure du document se fait en deux
étapes :

- La première étape cherche à classer des blocs en fonction de leurs
  caractéristiques. Par exemple on écrira qu'un bloc qui possède au moins
  N caractères par ligne, qui fait au moins M lignes et dont la fonte
  est la fonte la plus utilisée dans le document est un paragraphe.  
  Certaines caractéristiques permettent ensuite de définir des prototypes.
- La seconde vise à identifier les blocs restants par des recherches de
  similarités avec les prototypes identifiés lors de la première étape.

Avant de rechercher à reconstituer la structure du document, on calcule
pour chaque bloc la liste des caractéristiques suivantes :

- un booleen HAS_BULLET : s'il existe au moins une ligne du bloc
  qui ne commence pas par un caractère alphanumérique,
- un booleen FLAG_VERTICAL lorsque le bloc contient au moins une ligne
  de plus de 3 caractères plus haute que large,
- *bloc_size* : le nombre de caractères contenus dans le bloc,
- *max_line_size* : le plus grand nombre de caractères contenu dans une ligne,
- *not_numbers* : la proportion de caractères non-numériques dans le bloc,
- *last_character* : le dernier caractère du bloc (pour tester la présence
  de ponctuation),
- *nb_lines* : le nombre de lignes contenu dans le bloc,
- *font_class* : une caractérisation de la taille de la fonte du bloc, parmi
  les valeurs FONT_TINY, FONT_SMALL, FONT_DEFAULT, FONT_BIG et FONT_HUGE.
  Les seuils pour distribuer ces tailles, exprimés en pixels par rapport
  à la taille de la fonte par défaut, sont donnés dans le tableau
  FONT_THRESHOLDS (actuellement [ -3, -1, 0, 2, 7]),
- *alignment* : une caractérisation de la forme du bloc, déterminée
  pour les blocs de plus de deux lignes. Cet attribut contient l'une des valeurs
  suivantes :
  - ALIGN_JUSTIFIED : si le bloc fait au moins 3 lignes et que les
    arêtes gauches et droites des lignes sont alignées,
  - ALIGN_LEFT : si les arêtes gauches des lignes sont alignées mais pas les
    arêtes droites,
  - ALIGN_RIGHT : si les arêtes droites des lignes sont alignées mais pas
    les arêtes gauches,
  - ALIGN_CENTERED : si les milieux de lignes sont alignés mais pas les arêtes
    gauches et droites,
  - ALIGN_UNDEF dans tous les autres cas.

L'alignement des blocs s'entend modulo une constante exprimée en pixels
et nommée VERTICAL_ALIGMENT_THRESHOLD.


##### 2.4.7.1 Détermination des classes de blocs par des règles

La caractérisation d'un bloc s'appuie sur une série de règles. Lorsqu'un bloc
correspond à une des règles, on lui attribue une "classe" (titre, légende,
paragraphe, …). On dira que le bloc est "marqué".

Certaines règles permettent de définir des blocs prototypes. C'est à dire que
les caractéristiques du bloc permettent de déterminer sa classe sans ambiguïté.

La liste des règles est ordonnée. Lorsqu'une règle permet de marquer
un bloc, les règles suivantes ne sont pas testées sur ce bloc.

Voici la liste des règles pour déterminer les blocs prototypes  :

1. Des expressions régulières, données dans les listes CREDITS_REGEX,
  COPYRIGHT_REGEX et CONTACT_REGEX sont testées sur le contenu textuel
  des blocs. En cas de succès, le bloc est marqué BL_MISC.
  Il s'agit d'un paragraphe ou d'une note de pas de page identifiant les auteurs
  du BSV, ou les droits de propriété intellectuelle sur son contenu.
1. Un bloc sera marqué BL_PARAGRAPH si :
  - sa police majoritaire est la fonte par défaut,
  - son dernier caractère est un caractère de ponctuation,
  - sa plus longue ligne est supérieure à un seuil (PARAGRAH_LINE_SIZE,
    actuellement 40 caractères),
  - et s'il n'est ni centré ni aligné à droite.
1. Les expressions régulières contenues dans la liste CAPTION_REGEX sont
  testées ("crédit photo", "photo :", ...). En cas de succès le bloc
  est marqué BL_CAPTION (légende).


Voici la liste des règles pour déterminer d'autres classes sans générer de prototypes  :
- Les blocs marqués FLAG_VERTICAL sont marqués BL_MISC (miscellanous).
- Les blocs ne contenant pas de caractère alphanumérique sont ignorés ;
  il sont marqués BL_IGNORE.
- Si on ne trouve aucune légende, d'autres règles plus souples sont testées:
  - la fonte est plus petite que la fonte par défaut,
  - la longueur des lignes est inférieure à PARAGRAH_LINE_SIZE (40)
  - le bloc est centré ou aligné à gauche, ou indéterminé.
  - Les expressions régulières contenues dans LINK_REGEX (http(s)://)
    des blocs dont la fonte est au moins de la taille de la fonte par défaut
    sont testées. En cas de succès, le bloc est marqué BL_PARAGRAPH.  
    Ceci est dû au fait que les liens internet sont souvent en gras, ce qui
    les classe en titres. Or on peut supposer qu'un lien html n'est
    jamais dans un titre.

Une première série de blocs est classé à l'aide de ces règles.

La hiérarchie des titres est estimée par une série d'opérations décrites
dans la section suivante.

##### 2.4.7.2 Reconnaissance des titres

Les règles qui précèdent s'appliquent, à quelques exceptions près, à des blocs
dont la taille de fonte de caractères est au plus celle de la fonte par défaut.
La plupart des blocs ayant une grande fonte ne sont pas marqués à ce stade.

La reconnaissance des titres décrite dans cette section fait partie du processus
de détermination des classes de blocs par des règles.
Elle consiste à effectuer séquentiellement
les opérations suivantes :

- On parcourt les blocs non marqués ayant un nombre de caractères inférieur
  ou égal à un seuil, TITLE_SIZE_MAX (actuellement 60), ne se terminant pas
  par un caractère de ponctuation (point, point-virgule, virgule ou
  point d'exclamation) et ayant :
  - soit une fonte plus grande que celle par défaut,
  - soit une fonte de la même taille que celle par défaut mais ayant
    au moins un attribut de style (gras, italique, ...).

  Lorsqu'un tel bloc est rencontré, on relève sa fonte dans une liste F
  contenant la succession des fontes de titres.
- On considère ensuite les premiers éléments de la liste F :
  dans le cas où la fonte utilisée pour le(s) premier(s) bloc(s) de titre
  rencontré(s) n'est pas utilisée ailleurs, on considère que ces blocs
  représentent le titre du document. Il sont alors marqués BL_DOCUMENT_TITLE
  et cette fonte est retirée de F.
- À chaque fonte de la liste F, on attribue une ***taille relative*** de fonte,
  égale à
      S + 0,5.A - min(0,45 ; 0,1*B/3)
  où
  - S est la taille de fonte en pixels,
  - A est égale à 1 si la fonte a un attribut de style (gras, italique, ...), 0 sinon.
  - B le nombre de blocs ayant cette fonte.

  La formule privilégie l'hypothèse de la taille de la fonte sur les autres
  hypothèses. Pour une même taille de fonte, elle privilégie les fontes
  ayant un attribut (gras, italique, ...) sur celles qui n'en ont pas.
  Enfin, les fontes ayant le plus grand nombre d'occurences sont classées
  après celles, de même taille et même considération de style, qui en ont
  moins.   
  Ces hypothèses sont discutables ; elles donnent toutefois des résultats
  plutôt acceptables sur les BSV.

- Les fontes de titre sont ensuite triées par ordre décroissant de leur taille
  relative. On note C ce classement.
- Un traitement destiné à augmenter le niveau des titres est ensuite effectué
  sur C. Il consiste à mettre au même rang deux fontes successives de C
  pour lesquelles il n'y a aucune succession dans F, et a augmenter
  en conséquence le classement des fontes de C qui les suivent.

  Par exemple; si l'on a la structure suivante (où le numéro de fonte
  correspond à son ordre dans C) :
    - Titre de fonte 1
      - Titre de fonte 2
      - Titre de fonte 2
    - Titre de fonte 1
      - Titre de fonte 3
      - Titre de fonte 3

  alors on considère que les titres de fonte 3 sont aux même niveau que
  les titres de fonte 2. Il sont donc mis au même rang (2) dans le classement C.
- Les blocs de titre (ceux dont la fonte a été relevée pour constituer
  le tableau F) sont marqués de BL_TITLE_1 (h1) à BL_TITLE_5 (h5)
  en fonction du rang de leur fonte après le traitement précédent.
  Si ce rang est supérieur à 5, il a été choisi d'affecter la valeur
  BL_TITLE_5.  

  Le marquage des blocs de titre donne lieu à la génération de prototypes
  pour les cinq classes de titre.


##### 2.4.7.3  Classification des blocs restants

Lors de la première phase (détermination par des règles), des blocs prototypes
ont été identifiés pour certaines classes. De nouvelles données sont calculées
pour chaque classe à partir de ces blocs prototype :

- *nb_lines_per_bloc* : le nombre de lignes pour un bloc. Cet attribut est un
tableau où chaque cellule correspond au nombre de lignes d'un bloc prototype
donné. La valeur de la cellule du tableau prend l'une des valeurs suivantes :
  - courts (2 lignes ou moins)
  - moyens (entre 3 et 5 lignes)
  - longs (au moins 6 lignes)
- *block_size* : nombre moyen de caractères pour tous les blocs prototypes
  de la classe,
- *font_size* : la taille moyenne de la fonte pour tous les blocs prototypes
  de la classe,
- *alignment* : le nombre d'occurences des caractéristiques d'alignement
  (ALIGN_RIGHT, ...) des blocs prototypes de cette classe,
- *font* : la liste des fontes utilisées par tous les blocs prototypes
  de la classe considérée,
- *max_line_size* : la moyenne des longueurs maximales des lignes
  par bloc pour tous les blocs prototypes de la classe considérée,
- *not_numbers* : la proportion moyenne de caractère non-numériques
  pour tous les blocs prototypes de la classe,
- *nb_ponctuations* : le nombre de blocs de cette classe se terminant par
  un caractère de ponctuation.

Dans cette seconde phase, pour chaque bloc B qui n'a pas été marqué, on calcule
une valeur de similitude avec chacune des classes Ci :

    Sim(B, Ci) = (NLi + BSi + 6.0*FTi + 4.0*FSi + ALi + MLi + POi) / 15.0)

où :
- NLi (nombre de lignes par bloc) est le nombre de  blocs prototypes
  de la classe ayant la même longueur que le bloc B (court, moyen, long). Cette
  valeur est normalisée par rapport au nombre total de blocs prototype
  pour la classe considérée.
- BSi (block size) = 1 / max(1 ; | B['block_size'] - Ci['block_size'] |)
- FTi = 1 si la fonte de B est dans Ci['font'], 0 sinon,
- FSi (font_size) = 1 / max(1 ; 2.|B['font']['size'] - Pi['font_size']|)
- ALi (alignment) est le nombre de  blocs prototypes ayant le même alignement
  que le bloc B (justifié, centré, ...). Cette valeur est normalisée par le
  nombre total de blocs prototype appartenant à la classe Ci.
- MLi (max_line_size) = 1 / max(1 ; | B['max_line_size'] - Ci['max_line_size'] |)
- POi est la proportion de blocs prototypes se terminant (resp. ne se terminant
  pas) par un caractère de ponctuation si B se termine (resp. ne se termine pas)
  par un caractère de ponctuation.

Le bloc B se voit affecter la classe Ci pour laquelle la valeur de Sim(B,Ci)
est la plus élevée. Cette valeur est nommée "score" du bloc.

Une exception a lieu cependant pour les blocs dont les scores les plus élevés
correspondent à une classe de titre et dont la longueur est supérieure à
TITLE_SIZE_LIMIT (actuellement 140 caractères).
Ceux-ci sont marqués BL_PARAGRAPH. Il s'agit en général d'éléments
(de conclusion par exemple) mis en gras, ou de paragraphes d'une section
particulière écrits avec une police un peu plus grande (sections "À retenir"
par exemple).  
Étant donné que certains blocs de ces sections peuvent toutefois faire moins
de TITLE_SIZE_LIMIT caractères, une première passe de calcul de score a lieu
sur l'ensemble des blocs non marqués. Seuls les blocs dont le score les assimile
à un titre et comprenant plus de TITLE_SIZE_LIMIT caractères sont marqués
BL_PARAGRAPH et donnent lieu à l'ajustement du prototype BL_PARAGRAPH.
Une seconde passe de calcul de scores a ensuite lieu pour déterminer le type
des blocs restants.


### 2.5 Écriture des résultats

La sortie des résultats s'effectue dans un fichier html.

Un paramètre, DEBUG_PRINT, ajoute en commentaire la liste des fontes et
un attribut permettant d'identifier la fonte de chaque bloc. Un attribut
score est aussi ajouté aux blocs marqués lors de la deuxième phase.

Un paramètre PRINT_CSS ajoute dans l'entête un lien vers un fichier css
permettant de distinguer les différentes classes des blocs, et ajoute
aux blocs un attribut class.

Le tableau qui suit montre la manière dont sont écrites les différentes
classes de blocs :

|   Classe   |  Balise   |
| ---------- | --------- |
| BL_DOCUMENT_TITLE | Balise \<title\> de la partie \<head\> du document html,<br>puis balise \<h1\> dans le \<body\>. |
| BL_TITLE_1 | \<h1\>      |
| BL_TITLE_2 | \<h2\>      |
| BL_TITLE_3 | \<h3\>      |
| BL_TITLE_4 | \<h4\>      |
| BL_TITLE_5 | \<h5\>      |
| BL_TABLE   | \<table\>. Le contenu de la table est renvoyé par *camelot* |
| BL_CAPTION | \<figure\>\<figcaption\> |
| BL_BOTTOM_PAGE | \<footer\> |
| BL_TOP_PAGE    | \<header\> |
| BL_PARAGRAPH   | \<p\>  |
| BL_MISC        | \<p\>\<small\>  |

Dans l'écriture du texte en lui-même, un certain nombre de traitements sont
effectués :

- suppression des césures, en se basant sur le dictionnaire (on teste si un mot
  est présent dans le dictionnaire avant de supprimer une césure - les césures
  entre blocs subsistent cependant),
- remplacement des apostrophes obliques (*quotes* fermantes) par des apostrophes
  droites ('),
- remplacement des *doubles-quotes* et des guillemets à la française par des
  doubles apostrophes droites ("), ce traitement ainsi que le précédent
  perturbant les outils d'analyse lexicale,
- suppression des caractères "bizarres" (utilisés notamment pour les puces),
- suppression des espaces multiples.

Enfin, chacune des balises écrites est numérotée par l'ajout d'un
attribut nommé *blk*. Les balises sont ainsi numérotées par ordre
de lecture.

##### Remarques

- BL_PARAGRAPH et BL_MISC sont tous deux écrits sous la balise \<p\>.  
- Pour les en-têtes et les pieds de page de plusieurs lignes, les retour
  à la ligne sont préservés par le biais de balises \<br\>.
- Les puces ne sont pas restituées au sein de balises \<ul\> et \<li\>.
  S'il est aisé d'identifier une ligne qui ne commence pas par un caractère
  alphanumérique et d'en déduire que ce caractère est une puce, il y a une
  ambiguïté sur la fin de la puce. Il est difficile de distinguer le texte qui
  est dans la puce du texte qui est après la puce.  
  D'autre part le *html* n'accepte pas les balises \<ul\> à l'intérieur des
  balises \<p\>. Le découpage puces/paragraphe doit donc être très précis.  
  Parce que ce niveau de précision n'était pas envisageable, les puces sont
  signalées par un retour à la ligne et un tiret.  
  Les descriptions (lignes contenant deux points ':') sont aussi précédées
  d'un retour à la ligne.
