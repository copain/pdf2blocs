import re # To define some re.compile() regular expressions.
import enchant # To define DICT

CMD_PDFTOTEXT = '/usr/sbin/pdftotext'
CMD_PDFTOHTML = '/usr/sbin/pdftohtml'

# Licence to write at the end of the generated document.
# It's printed as is, so be careful to write clean html code. 
OUTPUT_LICENCE="""<!-- Licence -->
<a class="licence" href="http://ontology.inrae.fr/bsv/html/open-licence-2.0.html">
  <img class="etalab" src="http://ontology.inrae.fr/bsv/html/etalab.png" alt="Licence Ouverte Etalab" />
</a>"""


DEBUG_PRINT = False
PRINT_CSS = True
#URL_CSS = 'http://ontology.inrae.fr/bsv/html/bsv.css'
URL_CSS = 'https://rdf.codex.cati.inrae.fr/bsv/files/html/bsv.css'

# With version 22-06 (that is june, 2022), poppler pdftohtml
# returns different fontsize values, which completely messes up
# with our algorithm. So we'll use line height instead of fontsize
# when possible. But first result are not so good, so we'll keep
# the old algorithm, which will be running when USE_FONTSIZE is True.
USE_FONTSIZE = False

## Dictionaire :
#DICT = enchant.Dict("fr_FR")
DICT = enchant.DictWithPWL("fr_FR", "liste.de.mots.a.ajouter.au.dictionnaire.txt")
# Retire les lettres non accentuées, qui figurent dans ce dictionnaire.
for c in "bcdefghijklmnopqrstuvwxyzBCDEFGHIJKLMNOPQRESTUVWXYZ"[::]:
    DICT.remove(c)

#DICT = enchant.request_pwl_dict("liste.de.mots.francais.frgut.txt")


LEFT_THRESHOLD = 25 # In p2b_text_utils.add_lines() : the max horizontal space
                    # to consider aligned items to be on the same line.

HORIZONTAL_ALIGMENT_THRESHOLD = 8
VERTICAL_ALIGMENT_THRESHOLD = 14 # Avant, c'était 8

# Entre les positions des tables renvoyées par
# camelot et la position des blocs, il y a parfois des décalages.
# Du coup on agrandit le rectangle dans les 4 directions
# (TOP, BOTtom, LEFT, RIGHT)
# pour tester si un bloc est (entièrement contenu) dedans.
TABLE_LOC_THR_TOP = 2
TABLE_LOC_THR_BTM = 0.5
TABLE_LOC_THR_LEFT = 1
TABLE_LOC_THR_RIGHT = 1

# En dessous de ces valeurs, un bloc n'est pas considéré comme
# représentatif pour définir une colonne.
# (évite qu'une légende ou un tableau définisse une colonne de texte)
MIN_LINES_IN_COLUMN_BLOCK = 3
MIN_CAR_IN_COLUMN_BLOCK = 20

FLAG_NONE = 0x0000
FLAG_FIRST_BLOCK = 0x0001
FLAG_VERTICAL = 0x0002
# SMALL_FONT = 0x0001
# BIG_FONT = 0x0002 -> Unused
# PAGE_BOTTOM = 0x0004
MANY_FONTS = 0x0010
HAS_BULLET = 0x0020
IS_DESCRIPTION = 0x0040
# DEFAULT_FONT_SIZE = 0x0040
# TITLE_SMALLER_THAN_SUBTITLE = 0x0080

# Block classifications
BL_UNDEF = 0
BL_IGNORE = -1
BL_PARAGRAPH = 1
BL_BOTTOM_PAGE = 2
BL_TOP_PAGE = 3
BL_CAPTION = 4
BL_TABLE = 5
BL_LINK = 6 # Unused
BL_DOCUMENT_TITLE = 7
BL_TITLE_1 = 8
BL_TITLE_2 = 9
BL_TITLE_3 = 10
BL_TITLE_4 = 11
BL_TITLE_5 = 12
BL_CREDITS = 13   # Unused
BL_COPYRIGHT = 14 # Unused
BL_CONTACT = 15   # Unused
BL_MISC = 16

TITLE_CLASSES = [BL_TITLE_1, BL_TITLE_2, BL_TITLE_3, BL_TITLE_4, BL_TITLE_5, ]

BLOCKS_CLASSES = ['undefined', 'paragraph',
    'bottom_page', 'page_top', 'caption', 'table', 'see also',
    'doc_title', 'title 1', 'title 2', 'title 3', 'title 4',
    'title 5', 'credits', 'copyright', 'contact', 'misc',
    'ignore']

#BLOCK_PROTOTYPES = [None for dummy in range(len(BLOCKS_CLASSES))]


# Font categories are given by their threshold relative to default_font
FONT_TINY    = 0
FONT_SMALL   = 1
FONT_DEFAULT = 2
FONT_BIG     = 3
FONT_HUGE    = 4
FONT_THRESHOLDS = [ -3, -1, 0, 2, 7]
if not USE_FONTSIZE:
  #HEIGHT_THRESHOLDS = [ -5.0, -3.0, 0.0, 2.0, 7.0]
  FONT_THRESHOLDS = [ -5.0, -3.0, 0.0, 4.0, 9.0]
FONT_CLASSES = ['tiny', 'small', 'default', 'big', 'huge']

FONT_STYLE_REGULAR = 0
FONT_STYLE_IRREGULAR = 1

ALIGN_UNDEF = -1
ALIGN_CENTERED = 0
ALIGN_LEFT = 1
ALIGN_RIGHT = 2
ALIGN_JUSTIFIED = 3
ALIGN_CLASSES = ['centered', 'left', 'right', 'justified', 'undefined']

#### THRESHOLDS
# This is for 2-pass distance-recognition block types.
# On the 1st pass, we just mark blocks having scores > FIRST_PASS_THR,
# and prototypes are adjusted.
# On the second pass, all blocks have to be marked so we'll use
# the best score, even it's a really bad score.
FIRST_PASS_THR = 3.0/15.0


# Thresholds for S T Y L I S H E D titles
## To detect "stylished" titles, we count the number of characters per word
## in a line. If this number is less than STYLE_CHAR_PER_WORD_THRESHOLD,
## the "(maybe) stylished title" procedure is done.
STYLE_CHAR_PER_WORD_THRESHOLD = 3

## Lines having few characters per word are often numerical values in tables.
## So another condition to start the "maybe stylished" procedure is to have
## less than STYLE_CHAR_PROPORTION_THRESHOLD proportion of
## non-(alphabetic or space) characters in the line. The value 0.35 has
## quite good results and stands for about "one third"...
STYLE_CHAR_PROPORTION_THRESHOLD = 0.35

## This one is not a threshold but a list of particular cases for "T itles"
## having a bigger first letter. This is done particularly for "A il",
## because "A" and "il" are in a french dictionary, so they won't be
## concatenated if found at the beginig of a block. But "A il" doesn't have
## sense in french so it should be concatenated. This is why it's in this
## table. In the other hand, "CEREALES" is not found in the dictionary because
## it lacks two accents.
## Finally, "P OMMES" has nothing to do here because "ommes" is not in the
## dictionary and "pommes" is.
## Hope this make the use of this table understandable.
STYLE_CAPITAL_EXCEPTIONS = [
  ['A', r'ils?'],
  ['C', r'ereales?'],
  ['B', r'le'],
]



# These are thresholds for considering a block is shurely in a class

## "Normal" (default) paragrph
PARAGRAH_LINE_SIZE = 40 # At least PARAGRAH_LINE_SIZE characters
SMALL_LINES_LIMIT = 30  # Used to find captions : < PARAGRAH_LINE_SIZE
                        # could be too long.

# Si la proportion de caractères restants après suppression des
# NUMBERING_REGEX caractères, alors on considère qu'il n'y a que des nombres.
# (utilisé pour l'identification de tableaux)
NUMBERING_THRESHOLD = 0.3

# Below the TABLE_LENGTH value,
TABLE_LINE_SIZE = 25

TITLE_SIZE_MAX = 60 # Max size for titles in 1st phase
TITLE_SIZE_LIMIT = 140 # At the end, all title>TITLE_SIZE_LIMIT are marked <p>

TITLE_MIN_CHAR = 2 # To avoid “styled” bullet : we consider that a font never
                  # used for more than TITLE_MIN_CHAR characters per line
                  # is a kind of text styling and will take the next line's font

TITLE_MAX_LINES = 2 # a paragraph having more than TITLE_MAX_LINES lines
                    # is shurely not a title.

# SIMILARITY THRESHOLD : For line similarity (to assign font).
#   1.0 : no threshold, 0.0 : no similarity (score 0.0 is perfect match)
SIMILARITY_THRESHOLD = 1.0
# SIMILARITY_THRESHOLD = 0.6

# Celle là est un peu compliquée : Pour détecter la structure, on compte
# le nombre de successions d'un changement de police de caractères vers
# un autre (ex : la fonte 3 succède *2* fois à la fonte 8).
# Si ce nombre est trop peu élevé (<= NB_SUCCESSION_FOR_SAME) alors
# on considère que 8 n'est pas un titre de 3, et qu'ils sont au même niveau.
# Sinon, on considère que 8 est un niveau au-dessus dans la hiérarchie des
# titres, sous-titres, …
NB_SUCCESSION_FOR_SAME = 0

#### Regex
INDICES_EXPOSANTS_USUELS = [
  r'^(er|ère|ere)$', # 1er, 1ère, …
  r'^nde?$', # 2nd
  r'^(e|i?[eè]me)$', # 3ème, 4ieme, …
  r'^°$',
]

NUMBERING_REGEX = re.compile(r'[0-9,\.\-\+\(\)%°±~ ]')

PUNCTUATION_REGEX = re.compile(r'[\.\;\,\?\!]')

#CAPTION_REGEX = [ r'cr[ée]dit photo', r'photo ?:?', r'^\(', r'\)$' ]
CAPTION_REGEX = [ r'cr[ée]dit photo', r'photos? ?:?' ]

LINK_REGEX = [r'https?://', r'clique[rz]', r'\.gouv\.fr' ]

TABLE_SEPARATORS_REGEX = re.compile(r'[\n\t]')
HTML_BOLD_ITALIC_REGEX = re.compile(r'<\/?[bi] *>')

CREDITS_REGEX = [
  r'R[ée]daction ?:', r'R[ée]dacteurs? ?:', r'R[ée]dactrices? ?:',
  r'cr[ée]dits photos', # Au pluriel ici, singulier dans CAPTION
  r'Animation du réseau', r'Animateurs? ?:', r'Animatrices? ?:',
  r'Directrices? de publication', r'Directeurs? de publication',
  r'[ée]dit[ée] sous la responsabilit[ée]', r'action (co-)?pilot[ée]e par',
  r'cr[eé]dits issus de la redevance pour pollutions diffuses',
]

COPYRIGHT_REGEX = [ r'Reproduction int[ée]grale', 'Reproduction partielle' ]

CONTACT_REGEX = [r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"]

## --- Final transformations :

# Will be replaced with ' which is recognized by tree tagger
# Can be None.
APOSTROPHE_REGEX = r"[’]"

# Will be replaced with " which is recognized by tree tagger
# Can be None
DOUBLEQUOTE_REGEX = r'[“”«»]'

# https://gist.github.com/Alex-Just/e86110836f3f93fe7932290526529cd1
# Those characters will be substituted with spaces, and multiple spaces will
# be substituted with only one. This is done when printing results.
# TO_BE_REMOVED can be None.
TO_BE_REMOVED = re.compile(
    "["
    r"*#$~¤¥¦§¨©¬®\s_"
    "\U000002BE-\U0000FFFD"  # Symboles unicode divers
    "\U0001F1E0-\U0001F1FF"  # flags (iOS)
    "\U0001F300-\U0001F5FF"  # symbols & pictographs
    "\U0001F600-\U0001F64F"  # emoticons
    "\U0001F680-\U0001F6FF"  # transport & map symbols
    "\U0001F700-\U0001F77F"  # alchemical symbols
    "\U0001F780-\U0001F7FF"  # Geometric Shapes Extended
    "\U0001F800-\U0001F8FF"  # Supplemental Arrows-C
    "\U0001F900-\U0001F9FF"  # Supplemental Symbols and Pictographs
    "\U0001FA00-\U0001FA6F"  # Chess Symbols
    "\U0001FA70-\U0001FAFF"  # Symbols and Pictographs Extended-A
    "\U00002702-\U000027B0"  # Dingbats
    "\U000024C2-\U0001F251"
    "]+"
    )
