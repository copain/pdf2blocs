import sys

### Script pour faire tout le corpus :
# D=/home/phan/Boulot/Ontology/BSV/tmp/Corpus/Tests/GrandesCultures; for i in ${D}/*.pdf; do j=$( basename "$i" | sed -e 's/\.pdf//' ); echo $j; python pdf2blocks.py ${D}/$j > ${D}/${j}.html ; done

from p2b_functions import get_pdf2html

# +--------------------------------------------------------------+
# |                           main                               |
# +--------------------------------------------------------------+
if (len(sys.argv) < 1):
    print("-U-> Usage : python pdf2blocks.py <fichier_pdf>")
    sys.exit(-1)
html, dummy = get_pdf2html(sys.argv[1])
print(html, end='')
