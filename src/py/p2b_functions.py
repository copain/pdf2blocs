import xml.etree.ElementTree as ET
import enchant
import os
import sys
import re

# https://unix.stackexchange.com/questions/238180/execute-shell-commands-in-python
import subprocess

from io import StringIO
# from tabula import read_pdf # Résultats pas terribles.
import camelot

from p2b_utils import levenshtein
from p2b_config import *



# +--------------------------------------------------------------+
# |                         is_ind_exp                           |
# +--------------------------------------------------------------+
# Is it an indice or exposant ?
def is_ind_exp(str):
  for ie in INDICES_EXPOSANTS_USUELS:
      if re.match(ie, str):
          return True
  return False


# +--------------------------------------------------------------+
# |                        unaccent_fr                           |
# +--------------------------------------------------------------+
# Remove accents of french language of a lowercase string
def unaccent_fr(ch):
    ch = re.sub('[éèêë]','e',ch)
    ch = re.sub('[àâä]','a',ch)
    ch = re.sub('[ôö]','o',ch)
    ch = re.sub('[îï]','i',ch)
    ch = re.sub('[ùüû]','u',ch)
    return re.sub('ç','c',ch)

# +--------------------------------------------------------------+
# |                    is_equal_wo_accents                       |
# +--------------------------------------------------------------+
# Return true if ch1 and ch2 are equal accentless and caseless.
def is_equal_wo_accents(ch1, ch2):
    return unaccent_fr(ch1.strip().lower()) == unaccent_fr(ch2.strip().lower())



# +--------------------------------------------------------------+
# |                         check_dict                           |
# +--------------------------------------------------------------+
# Checks if it finds the word in DICT dictionary. If not, searches into
# DICT suggestions if it finds the same word without accent.
# Returns True if finds something.
def check_dict(word_to_test):
    if len(word_to_test) == 0:
        return False
    if DICT.check(word_to_test):
        return True
    for sug in DICT.suggest(word_to_test):
        if is_equal_wo_accents(sug, word_to_test):
            return True
    return False


# +--------------------------------------------------------------+
# |                       get_pdftotext                          |
# +--------------------------------------------------------------+
def get_pdftotext(filename):
  # Calls pdftotext and retreive standard output in a string (o)
  basename = os.path.splitext(filename)[0]
  cmd = [CMD_PDFTOTEXT, '-bbox-layout', '-eol', 'unix', '%s.pdf' % basename, '-']
  proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  o, e = proc.communicate()
  if (proc.returncode != 0):
    return []

  # Parse xml code and create block table.
  xml = o.decode('utf8')
  xml = re.sub(r'[\x00-\x09\x0b-\x1f]', '', xml)
  root = None
  try:
    root = ET.fromstring(xml)
  except Exception as e:
      return []

  page_num = 0
  flow_num = 0
  blocks = []
  for body in root:
    if (body.tag.endswith('body')):
      for doc in body:
        if (doc.tag.endswith('doc')):
          for page in doc:
            if (page.tag.endswith('page')):
              page_num += 1
              pwidth = float(page.get('width'))
              pheight = float(page.get('height')) # We need it because
                            # camelot uses y coordinate from the bottom
              for fl in page:
                if (fl.tag.endswith('flow')):
                  flow_num += 1
                  for bloc in fl:
                    if (bloc.tag.endswith('block')):
                      bl = {'page': page_num, 'flow': flow_num, 'lines': [],
                            'flags': FLAG_NONE,
                            'x_min': float(bloc.get('xMin')),
                            'x_max': float(bloc.get('xMax')),
                            'y_min': float(bloc.get('yMin')),
                            'y_max': float(bloc.get('yMax')),
                            'pageheight' : pheight, 'pagewidth' : pwidth,
                            }
                      first_line = True
                      for line in bloc:
                        if (line.tag.endswith('line')):
                          h = float(line.get('yMax')) - float(line.get('yMin'))
                          w = float(line.get('xMax')) - float(line.get('xMin'))
                          li = { 'text': '', 'height': h, 'words': [],
                            'flags': FLAG_NONE,
                            'x_min': float(line.get('xMin')),
                            'x_max': float(line.get('xMax')),
                            'y_min': float(line.get('yMin')),
                            'y_max': float(line.get('yMax')),
                          }

                          last_word = ''
                          hspaces = []
                          last_x = -1
                          nb_cars = 0
                          nb_words = 0
                          no_space_pls = False
                          for word in line:
                            if (word.tag.endswith('word')) and word.text is not None:
                              hword = float(word.get('yMax')) - float(word.get('yMin'))
                              li['words'].append({'height': hword, 'text': word.text})

                              # Some statistics to detect stlylished lines
                              if last_x <= 0:
                                  last_x = float(word.get('xMax'))
                              else:
                                  hspaces.append(float(word.get('xMin')) - last_x)
                                  last_x = float(word.get('xMax'))
                              #print("#### [%s]" % word.text)
                              l = len(re.sub(r'\W', '', word.text))
                              if l > 0:
                                  nb_words +=1
                              nb_cars += len(word.text)

                              if no_space_pls:
                                  # no_space_pls indicates that the previous
                                  # word was the first of the block and is
                                  # a single capital letter. So it may be
                                  # a "T itle" effect on the 1st letter.
                                  particular_case = False
                                  for couple in STYLE_CAPITAL_EXCEPTIONS:
                                      particular_case = re.match(couple[0], li['text'], flags = re.IGNORECASE) \
                                        and re.match(couple[1], word.text, flags = re.IGNORECASE)
                                      if particular_case:
                                          break
                                  word_to_test = ("%s%s" % (li['text'], word.text)).split(" ")[-1]
                                  if particular_case:
                                      li['text'] = "%s%s" % (li['text'], word.text)
                                  elif DICT.check(li['text'].split(" ")[-1]) \
                                    and DICT.check(word.text):
                                      li['text'] = "%s %s" % (li['text'], word.text)
                                  elif check_dict(word_to_test) or \
                                    check_dict(re.sub(r'\W','',word_to_test)):
                                      # Quelques explications sur le test qui précède :
                                      # Il peut sembler redondant mais ne l'est
                                      # pas pour les apostrophes.
                                      li['text'] = "%s%s" % (li['text'], word.text)
                                  else:
                                      li['text'] = "%s %s" % (li['text'], word.text)
                                  no_space_pls = False
                              elif last_word.isdecimal() and is_ind_exp(word.text):
                                  li['text'] = "%s%s" % (li['text'], word.text)
                              elif first_line and len(li['text']) == 0 and \
                                re.match(r'^[A-ZÉÈÂÊÄËÏÎÔÖÙÜÛ]$', word.text.strip()):
                                  # "T itle" case :
                                  no_space_pls = True
                                  li['text'] = "%s %s" % (li['text'], word.text)
                              else:
                                  li['text'] = "%s %s" % (li['text'], word.text)

                              li['text'] = li['text'].strip()
                              last_word = word.text

                          # --- End "for word in line"

                          if len(li['text']) > 3 and w < h: # Probably vertical text
                              li['height'] = w
                              bl['flags'] |= FLAG_VERTICAL

                          ### Is it a "S T Y L I S H" or badly justified line ?
                          #   If yes, we'll redo it a different way

                          # If there are too much special characters or digits,
                          # the number of short words is certainly normal.
                          nb_special_car = len(li['text'])
                          nb_special_car -= len(re.sub(r"[\W\d]", '', re.sub(r'\s','A',li['text'])))

                          if nb_words > 1 and (float(nb_cars) / float(nb_words)) < STYLE_CHAR_PER_WORD_THRESHOLD \
                            and float(nb_special_car) / float(len(li['text'])) < STYLE_CHAR_PROPORTION_THRESHOLD:

                              hspaces.sort()
                              # Let's search if there are 2 kinds of spaces
                              # (space between letters and space between words)
                              left = 0
                              right = len(hspaces) - 1
                              diff_space = 0.25
                              while right > (left + 1):
                                  dl = hspaces[left+1] - hspaces[left]
                                  dr = hspaces[right] - hspaces[right-1]
                                  if dl < dr:
                                      left += 1
                                      diff_space = max(diff_space, dl)
                                  else:
                                      right -= 1
                                      diff_space = max(diff_space, dr)
                              if (hspaces[right] - hspaces[left]) <= diff_space: # 1 space size
                                car_space = hspaces[-1] + 0.1
                              else :
                                car_space = (hspaces[right] + hspaces[left]) / 2.0
                              last_x = -1
                              ltxt = wo = ''
                              words_list = []
                              hword = -1.0
                              for word in line:
                                if (word.tag.endswith('word')):
                                  hword = max(hword, float(word.get('yMax')) - float(word.get('yMin')))

                                  if last_x <= 0:
                                      last_x = float(word.get('xMax'))
                                      hs = 0
                                      if word.text is not None:
                                        wo = word.text.strip()
                                      else:
                                        wo = ""
                                  else:
                                      hs = float(word.get('xMin')) - last_x
                                      last_x = float(word.get('xMax'))
                                      if hs > car_space:
                                          ltxt = ("%s %s" % (ltxt, wo)).strip()
                                          words_list.append({'height': hword, 'text': wo.strip()})
                                          hword = -1
                                          if word.text is not None:
                                            wo = word.text.strip()
                                          else:
                                            wo = ""
                                      else:
                                        if word.text is not None:
                                          wo = "%s%s" % (wo, word.text.strip())
                              ltxt = ("%s %s" % (ltxt, wo)).strip()

                              if len(wo.strip()) > 0:
                                  words_list.append({'height': hword, 'text': wo.strip()})
                              nb_ok_new = 0
                              ls = re.sub(r' +', ' ', re.sub(r"[^'’\w]", ' ', ltxt)).strip().split(' ')
                              for wo in ls:
                                  if len(re.sub(r'\d','',wo)) > 0: # Don't check numbers
                                    if check_dict(wo): nb_ok_new += 1
                              prop_ok_new = float(nb_ok_new) / float(len(ls))
                              nb_ok_old = 0
                              ls = re.sub(r' +', ' ', re.sub(r"[^'’\w]", ' ', li['text'])).strip().split(' ')
                              for wo in ls:
                                  if len(re.sub(r'\d','',wo)) > 0: # Don't check numbers
                                    if check_dict(wo): nb_ok_old += 1
                              prop_ok_old = float(nb_ok_old) / float(len(ls))
                              if prop_ok_new > prop_ok_old :
                                  li['text'] = ltxt
                                  li['words'] = words_list

                          bl['lines'].append(li)
                          first_line = False
                      if len(bl['lines']) == 0:
                        bl['line_height'] = -1.0
                      else:
                        bl['line_height'] = \
                          float(sum([l['y_max']-l['y_min'] for l in bl['lines']])) / float(len(bl['lines']))
                      blocks.append(bl)
  return blocks


# +--------------------------------------------------------------+
# |                       get_pdftohtml                          |
# +--------------------------------------------------------------+
def get_pdftohtml(filename):
  basename = os.path.splitext(filename)[0]
  cmd = [CMD_PDFTOHTML, '-xml', '-nodrm', '-i', '-stdout', '%s.pdf' % basename]
  proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  o, e = proc.communicate()
  if (proc.returncode != 0):
    #print('-S-> Command pdftohtml returned an error :')
    #print('     '  + e.decode('utf8'))
    return None, None

  # Parse xml code and create block table.
  try:
    xml = o.decode('utf8')
  except UnicodeDecodeError:
    xml = o.decode('unicode_escape')
  
  # Remove <b> and <i> tags which give parse errors and are not used.
  xml = HTML_BOLD_ITALIC_REGEX.sub('', xml)
  root = None
  try:
    root = ET.fromstring(xml)
  except Exception as e:
    return [], []

  fontspec = []
  segments = []
  for page in root:
    if (page.tag.endswith('page')):
        pg = int(page.get('number'))
        pheight = int(page.get('height'))
        pwidth = int(page.get('width'))
        for tg in page:
            if (tg.tag.endswith('fontspec')):
                fontspec.append({
                    'id': int(tg.get('id')),
                    'size': int(tg.get('size')),
                    'family': tg.get('family'),
                    'color': tg.get('color'),
                    'nb_cars': 0, 'height': 0.0, 'nb_seg': 0.0,
                })
            elif (tg.tag.endswith('text')):
                fnt = int(tg.get('font'))
                top = int(tg.get('top'))
                left = int(tg.get('left'))
                width = int(tg.get('width'))
                height = int(tg.get('height'))
                while (tg.text is None) and (len(tg) > 0):
                    tg = tg[0] # remove html style tags (like <b>, …)
                if (tg.text is not None):
                    li = "%s" % (tg.text)
                    if (len(li.strip()) > 0):
                        segments.append({'page': pg, 'font': fnt,
                            'top': top, 'left': left,
                            'width': width, 'height': height,
                            'pagewidth' : pwidth, 'pageheight': pheight,
                            'text': li.strip()
                        })
                        # Find font in fontspec
                        for font in fontspec:
                            if font['id'] == fnt: break
                        font['nb_cars'] += len(li.strip())
                        font['height'] += height
                        font['nb_seg'] += 1.0
  for font in fontspec:
    if font['nb_seg'] > 0.0:
      font['height'] /= font['nb_seg']
  return segments, fontspec


# +--------------------------------------------------------------+
# |                       get_pdftohtml                          |
# +--------------------------------------------------------------+
def get_tables(filename, list_pages):
  """
Returns a list containing tables found by camelot. Each element
is an instance of https://camelot-py.readthedocs.io/en/master/api.html#camelot.core.TableList

It does a selection of results (accuracy > 90.0 and whitespace < 50.1).
It also doesn't select single column tables.

It doesn't try the option process_background=True to avoid doing
the job twice, but maybee we should do it.

We use poppler as a backend because we already use pdftohtml and
pdftotext. But ghostscript is faster.
  """
  ACCURACY_THR = 90.0
  WHITESPACE_THR = 50.0
  
  pdf_file = '%s.pdf' % os.path.splitext(filename)[0]
  tables = []
  tl = []
  try:
    #tl = camelot.read_pdf(pdf_file, backend="poppler", flavor='stream', pages="all")
    #tl = camelot.read_pdf(pdf_file, backend="poppler", pages="all", split_text=True)
    tl = camelot.read_pdf(pdf_file, backend="poppler", pages="all", shift_text=[''], strip_text='.\n')
  except:
    # e.g : "PyPDF2.errors.PdfReadError: Could not read malformed PDF file"
    pass # tl is [] so should return []
  for t in tl:
    n_tabs = 0
    if t.shape[1] == 1:
      for lines in t.cells:
        n_tabs = max(n_tabs, len(TABLE_SEPARATORS_REGEX.split(lines[0].text)))
    if (t.accuracy > ACCURACY_THR) and (t.whitespace < WHITESPACE_THR) \
      and ((t.shape[1] > 1) or ((n_tabs > 0) and (t.shape[0] > 1))):
      c = []
      tt = { 'cells': t.cells, 'page': t.page, 'table': t # Maybe Not necessary
        }
      for l in t.cells: c += l
      tt['x_min'] = min([x.x1 for x in c])
      tt['x_max'] = max([x.x2 for x in c])
      tt['y_min'] = min([x.y1 for x in c])
      tt['y_max'] = max([x.y2 for x in c])
      tables.append(tt)
  return tables


# +--------------------------------------------------------------+
# |                   get_default_font_size                      |
# +--------------------------------------------------------------+
#def get_default_font_size(fontspec):
#  sizes = {}
#  max_cars = 0
#  size_max_cars = 42 # Doesn't matter : it's going to change
#  for f in fontspec:
#      if sizes.get(f['size']) is None:
#          sizes[f['size']] = f['nb_cars']
#      else:
#          sizes[f['size']] += f['nb_cars']
#      if sizes[f['size']] > max_cars:
#          max_cars = sizes[f['size']]
#          size_max_cars = f['size']
#  return size_max_cars


# +--------------------------------------------------------------+
# |                      mark_small_fonts                        |
# +--------------------------------------------------------------+
# RQ : Also marks bullet lines
def mark_small_fonts(blocks, default_font_size):
    for b in blocks:
        for l in b['lines']:
            if (round(l['height']) < default_font_size):
                l['flags'] |= SMALL_FONT
            if len(re.sub(r'\W','', l['text'])) == 0:
                l['flags'] |= IS_BULLET


# +--------------------------------------------------------------+
# |                      mark_page_bottom                        |
# |                             and                              |
# |                        mark_page_top                         |
# +--------------------------------------------------------------+
def mark_page_btotp(pages, ndx, increment, MARK):
    if len(pages) == 1: return
    end = False
    k = range(2,len(pages)+1)
    if len(pages) == 2:
        k = range(1,len(pages)+1)
    while not end:
        li = None
        for p in k:
          try:
            if li == None:
                li = "".join([re.sub(r'[^a-zA-Z]', '', l['text']) \
                        for l in pages[p]['blocks'][ndx]['lines']])
            else:
                end |= (li != "".join([re.sub(r'[^a-zA-Z]', '', l['text']) \
                    for l in pages[p]['blocks'][ndx]['lines']]))
          except Exception as e:
            end = True

        if not end:
            for p in k:
              if pages[p]['blocks'][ndx]['class'] != BL_TABLE:
                pages[p]['blocks'][ndx]['class'] = MARK

            if len(pages) > 2:
                try:
                  if li == "".join([re.sub(r'[^a-zA-Z]', '', l['text']) \
                    for l in pages[1]['blocks'][ndx]['lines']]):
                      if pages[1]['blocks'][ndx]['class'] != BL_TABLE:
                        pages[1]['blocks'][ndx]['class'] = MARK
                except Exception as e:
                    pass # <--- Booooo : VERY BAD !!!
            ndx += increment
            if len(pages) > 0:
              end = (abs(ndx) > min([len(p['blocks']) for p in pages.values()]))
            else:
              end = 1

def mark_page_bottom(pages):
    mark_page_btotp(pages, -1, -1, BL_BOTTOM_PAGE)

def mark_page_top(pages):
    mark_page_btotp(pages, 0, 1, BL_TOP_PAGE)


# +--------------------------------------------------------------+
# |                         get_lines                            |
# +--------------------------------------------------------------+
# Extract lines from 'text' attribute returned by get_pdftohtml and associates
# a font id (and the page number), which is the font used by the higher number
# of characters of the line.
# Does a column splitting considering the value of LEFT_THRESHOLD
def get_lines(segments, fontspec):
    last_top = -1
    line_no = -1
    last_right = 0
    if len(segments) == 0:
        return []
    for txt in segments:
        if (txt['top'] == last_top) and ((txt['left'] - last_right) <= LEFT_THRESHOLD):
            txt['line'] = line_no
        elif is_ind_exp(txt['text'].strip()):
            txt['line'] = line_no
        else:
            line_no += 1
            txt['line'] = line_no
            last_top = txt['top']
        last_right = txt['left'] + txt['width']

    for f in fontspec:
        if 'same_line' not in f:
            f['same_line'] = []

    lines = []
    last_line = -2
    li = ''
    xmin = 0
    xmax = 0
    ymin = 0
    ymax = 0
    fnt = {}
    page_num = segments[0]['page']
    for txt in segments:
        if (txt['line'] != last_line) or (txt == segments[-1]):
            if (len(li.strip()) > 0):
                fnt_no = -1; max_car = 0;
                for f in fnt.keys():
                    if (fnt[f] > max_car):
                        max_car = fnt[f]
                        fnt_no = f
                lines.append({ 'text': li.strip(),
                    'exact_match': False, # Used by guess_fonts()
                    'xmin':xmin, 'xmax':xmax, 'ymin':ymin, 'ymax':ymax,
                    'pagewidth' : txt['pagewidth'],
                    'pageheight' : txt['pageheight'],
                    'most_used_font': fnt_no,
                    'nb_fonts': len(fnt),
                    'page': page_num})
            li = txt['text'].strip()
            xmin = txt['left']
            xmax = xmin + txt['width']
            ymin = txt['top']
            ymax = ymin + txt['height']
            last_line = txt['line']
            for fi1 in fnt.keys():
                for fi2 in fnt.keys():
                    if fi1 != fi2:
                        f1 = next(it for it in fontspec if it['id'] == int(fi1))
                        f2 = next(it for it in fontspec if it['id'] == int(fi2))
                        if (f2['id'] not in f1['same_line']):
                            f1['same_line'].append(f2['id'])
                            f2['same_line'].append(f1['id'])
            fnt = {}
            fnt[txt['font']] = len(li.strip())
        else:
            if txt['left'] < xmin:
              xmin = txt['left']
            if txt['left'] + txt['width'] > xmax:
              xmax = txt['left'] + txt['width']
            if txt['top'] < ymin:
              ymin = txt['top']
            if txt['top'] + txt['height'] > ymax:
              yamx = txt['top'] + txt['height']
            if (is_ind_exp(txt['text'])):
                li = "%s%s" % (li, txt['text'].strip())
            else:
                li = "%s %s" % (li, txt['text'].strip())
            if (fnt.get(txt['font']) is None):
                fnt[txt['font']] = len(txt['text'].strip())
            else:
                fnt[txt['font']] += len(txt['text'].strip())
        page_num = txt['page']
    return lines

# +--------------------------------------------------------------+
# |                        guess_fonts                           |
# +--------------------------------------------------------------+
# Tries to guess fontspec of each line into blocks list.
# It calculates the levenshtein distance with every intersecting segment 
# and assigns the best matching score's font.
def guess_fonts(blocks, segments, fontspec):
    lines = get_lines(segments, fontspec)
    ndx_lines = [0,] # Indexation des indices de line par numéro de page
    for ndx in range(1, len(lines)):
        if (lines[ndx-1]['page'] != lines[ndx]['page']):
            ndx_lines.append(ndx)
    ndx_lines.append(len(lines))

    for f in fontspec:
        f['nb_lines'] = 0
        f['dist_sum'] = 0

    for bl in blocks:
        bl_max_car = bl_font = -1
        block_fonts = {}
        for l in bl['lines']:
            if (len(l['text']) > 0) and bl['page'] < len(ndx_lines):
                min_dist = len(l['text'])
                min_score = 1.0
                font_sel = -1
                line_no = -1
                for i in range(ndx_lines[bl['page']-1], ndx_lines[bl['page']]):
                    ll = lines[i]
                    llxmin = bl['pagewidth'] * float(ll['xmin']) / float(ll['pagewidth'])
                    llxmax = bl['pagewidth'] * float(ll['xmax']) / float(ll['pagewidth'])
                    llymin = bl['pageheight'] * float(ll['ymin']) / float(ll['pageheight'])
                    llymax = bl['pageheight'] * float(ll['ymax']) / float(ll['pageheight'])
                    minright = min(l['x_max'], llxmin)
                    maxleft = max(l['x_min'], llxmax)
                    minbottom = min(l['y_max'], llymin)
                    maxtop = max(l['y_min'], llymax)
                    
                    if (not lines[i]['exact_match']) and (len(lines[i]['text']) > 0) \
                      and (minright < maxleft) and (minbottom < maxtop):
                        d = levenshtein(l['text'], lines[i]['text'])
                        if (d == 0):
                            min_dist = 0
                            min_score = 0.0
                            font_sel = lines[i]['most_used_font']
                            lines[i]['exact_match'] = True
                            line_no = i
                            break;
                        score = float(d) / float(max(len(l['text']), len(lines[i]['text'])))
                        if (score <= SIMILARITY_THRESHOLD):
                            if (d < min_dist):
                                min_dist = d
                                min_score = score
                                font_sel = lines[i]['most_used_font']
                                line_no = i
                l['font'] = font_sel
                if (font_sel >= 0):
                  fnt = next(it for it in fontspec if it['id'] == font_sel)
                  fnt['nb_lines'] +=1
                  fnt['dist_sum'] += min_dist
                l['score'] = min_score # For debuggin purpose
                l['dist'] = min_dist   #    idem.
                l['line_no'] = line_no # idem. Stores the "similar line" number
                if line_no >= 0:
                    if (lines[line_no]['nb_fonts'] > 1):
                        l['flags'] |= MANY_FONTS
                if block_fonts.get(font_sel) is None:
                    block_fonts[font_sel] = len(l['text'])
                else:
                    block_fonts[font_sel] += len(l['text'])
                if block_fonts[font_sel] > bl_max_car:
                    bl_max_car = block_fonts[font_sel]
                    bl_font = font_sel
            # - End if len(l['text']) > 0
        # - End loop on lines
        if bl_font < 0:
            bl_max_car = bl_font = -1
            block_fonts = {}
            for l in bl['lines']:
                s = round(l['height'])
                if block_fonts.get(s) is None: block_fonts[s] = 0
                block_fonts[s] += len(l['text'])
                if block_fonts[s] > bl_max_car:
                    bl_max_car = block_fonts[s]
                    bl_font = s
            bl['font'] = { 'family': None, 'id': -1, 'size': bl_font,
                           'nb_blocks': -1}
        else:
            bl['font'] = next(f for f in fontspec if f['id'] == bl_font)
        bl['nb_fonts'] = len(block_fonts)
    # - End loop on blocks



# +--------------------------------------------------------------+
# |                    replace_block_fonts                       |
# +--------------------------------------------------------------+
# Adds a 'short_font' attribute to lines which gives another font value which
# doesn't care about style (bold, …).
# RK: def_size is default_font_size, used to mark SMALL_FONT flag.
def replace_block_fonts(blocks, fontspec, def_size):
    for i in range(0, len(fontspec) - 1):
        for j in range(i+1, len(fontspec)):
          if (fontspec[j].get('replaceWith') is None):
            if (fontspec[j]['id'] in fontspec[i]['same_line']):
                if fontspec[i].get('replaceWith') is None:
                    fontspec[j]['replaceWith'] = fontspec[i]['id']
                else:
                    fontspec[j]['replaceWith'] = fontspec[i]['replaceWith']
    for bl in blocks:
        for l in bl['lines']:
            if (l['font'] < 0):
                f = None
            else:
                f = next(it for it in fontspec if it['id'] == l['font'])
            if (f is None) or (f.get('replaceWith') is None):
                l['short_font'] = l['font']
            else:
                l['short_font'] = f.get('replaceWith')
            if (f is not None):
                f = next(it for it in fontspec if it['id'] == l['short_font'])
                if (f['size'] < def_size):
                    l['flags'] |= SMALL_FONT
                if (f['size'] == def_size):
                    l['flags'] |= DEFAULT_FONT_SIZE


# +--------------------------------------------------------------+
# |                        get_columns                           |
# +--------------------------------------------------------------+
# Retourne la liste des x_min des blocs les plus à gauche de chaque colonne
# détectée. S'il n'y a pas de BL_PARAGRAPH, renvoie [0] (pour que
# les algos qui traitent les autres blocs fonctionnent)
def get_columns(blocks, default_font_size):
    blp = [b for b in blocks if b['nb_lines'] >= MIN_LINES_IN_COLUMN_BLOCK and \
        b['font']['size'] <= default_font_size \
        and b['max_line_size'] >= MIN_CAR_IN_COLUMN_BLOCK]

    if len(blp) == 0:
        return [0]
    columns = [min([b['x_min'] for b in blocks])]
    col_len = 0
    while col_len != len(columns):
        col_len = len(columns)
        rights = [b['x_max'] for b in blp if b['x_min'] >= columns[-1]]
        if len(rights) > 0:
            min_rights = min(rights)
            lefts = [b['x_min'] for b in blocks if b['x_min'] > min_rights \
            and b['max_line_size'] >= MIN_CAR_IN_COLUMN_BLOCK ]
            if len(lefts) > 0:
                columns.append(min(lefts))
    return columns


# +--------------------------------------------------------------+
# |                        compute_lrud                          |
# +--------------------------------------------------------------+
# Compute 'left', 'right', 'up' and 'down' on blocks,
# where those are the minimum distance to another block
# on left, on right, ...
# Has a negative value where no other block on the direction.
# Should be computed before expand_blocks(...)
def compute_lrud(page_blocks):
    for b in page_blocks:
        b['left'] = b['right'] = b['top'] = b['down'] = -1
    for b in page_blocks:
        for ob in page_blocks: # ob : other block
            if ob is not b:
                if ob['y_min'] < b['y_max'] and ob['y_max'] > b['y_min']: # Vertically aligned
                    if ob['x_min'] >= b['x_max']: # ob on right
                        dist = ob['x_min'] - b['x_max']
                        if b['right'] < 0:
                            b['right'] = dist
                            b['right_block'] = ob
                        elif b['right'] > dist:
                            b['right'] = dist
                            b['right_block'] = ob
                    if ob['x_max'] < b['x_min']: # ob on left
                        dist = b['x_min'] - ob['x_max']
                        if b['left'] < 0:
                            b['left'] = dist
                            b['left_block'] = ob
                        elif b['left'] > dist:
                            b['left'] = dist
                            b['left_block'] = ob
                if ob['x_min'] <= b['x_max'] and ob['x_max'] >= b['x_min']: # Horizontally aligned
                    if ob['y_min'] >= b['y_max']: # ob under b
                        dist = ob['y_min'] - b['y_max']
                        if b['down'] < 0:
                            b['down'] = dist
                            b['down_block'] = ob
                        elif b['down'] > dist:
                            b['down'] = dist
                            b['down_block'] = ob
                    if ob['y_max'] < b['y_min']: # ob on top of b
                        dist = b['y_min'] - ob['y_max']
                        if b['top'] < 0:
                            b['top'] = dist
                            b['top_block'] = ob
                        elif b['top'] > dist:
                            b['top'] = dist
                            b['top_block'] = ob


# +--------------------------------------------------------------+
# |                       expand_blocks                          |
# +--------------------------------------------------------------+
# Make the big-fonted blocs (titles?) take the most space they can, growing
# right without intersecting another block.
# RQ : this is to be applied on a list of blocs in the SAME PAGE
def expand_blocks(page_blocks, default_font_size):
    pb = page_blocks # for shorter writing
    if len(pb) == 0: return
    max_x = max([b['x_max'] for b in pb])
    min_x = min([b['x_min'] for b in pb])
    for b in pb:
      if b['font']['size'] > default_font_size:
        b['xx_max'] = max_x
        b['xx_min'] = min_x

    for block in pb:
      if block['font']['size'] > default_font_size:
        for b in pb:
            if b['y_min'] < block['y_max'] and b['y_max'] > block['y_min']:
               if b['x_min'] >= block['x_max'] and block['xx_max'] > b['x_min']:
                   block['xx_max'] = max(block['x_max'], b['x_min'] - (VERTICAL_ALIGMENT_THRESHOLD/2))
               if b['x_max'] <= block['x_min'] and block['xx_min'] < b['x_max']:
                   block['xx_min'] = b['x_max']

    for b in pb:
      if b['font']['size'] > default_font_size:
          b['x_max'] = b['xx_max']
          #b['x_min'] = b['xx_min']
          del b['xx_max']
          del b['xx_min']

# +--------------------------------------------------------------+
# |                        sort_blocks                           |
# +--------------------------------------------------------------+
def sort_blocks(blocks, columns, default_font_size):
    PRINT_SORT = False
    ordered_blocks = []
    nb_blocks = 0
    for b in blocks:
        b['treat'] = (b['class'] == BL_IGNORE \
            or b['class'] == BL_BOTTOM_PAGE \
            or b['class'] == BL_TOP_PAGE \
            or (b['class'] == BL_TABLE and len(b['lines']) == 0))
        if not b['treat']:
            nb_blocks += 1

##        c = 1
##        while columns[c] <= b['x_min']: c += 1
##        b['col_min'] = c - 1
##        while columns[c] < b['x_max'] - HORIZONTAL_ALIGMENT_THRESHOLD: c += 1
##        b['col_max'] = c
##        # WARNING : if block in column 1, col_min ← 0 and col_max ← 1.
        b['col_min'] = 0
        b['col_max'] = 1
        if len(columns) > 0:
          c = 1
          while (c < len(columns)) and (columns[c] <= b['x_min']): c += 1
          if (c ==  len(columns)): c -= 1
          b['col_min'] = c - 1
          while columns[c] < b['x_max'] - HORIZONTAL_ALIGMENT_THRESHOLD: c += 1
          b['col_max'] = c
          # WARNING : if block in column 1, col_min ← 0 and col_max ← 1.
        b['temp_left'] = b['col_min']

    for b in blocks:
        if b['class'] == BL_TOP_PAGE:
            ordered_blocks.append(b)
            nb_blocks += 1

    # curr_col is for "current columns". It is designed by its index in the
    # column table, and (min,max) values so it can be used to tell for example,
    # "we're looking for blocks contained into columns 0 to 2" (there, min is 0
    # and max is 2).
    curr_col_min = 0
    curr_col_max = 1

    while len(ordered_blocks) != nb_blocks:
        if PRINT_SORT:
          print("===> %d / %d" % (len(ordered_blocks), len(blocks)))

        # On cherche la hauteur du plus haut des blocs dans la colonne
        bl_top = []
        while len(bl_top) == 0:
            if PRINT_SORT:
                print("* [%d,%d]" % (curr_col_min, curr_col_max))

            # Tant qu'on n'en trouve pas, on élargit les colonnes.
            # Si on n'en trouvait toujours pas c'est qu'il y a un gros bug :
            # donc on choisit de faire planter. PS : ça n'est jamais arrivé
            #                                        dans le corpus.
            # WARNING : bl_top is badly named. It doesn't contain the top blocks
            #   but all blocks in current column(s)
            bl_top = [b for b in blocks if not b['treat'] and \
                b['temp_left'] >= curr_col_min and \
                b['temp_left'] < curr_col_max]
            if len(bl_top) == 0:
                if (curr_col_max < len(columns) - 1):
                    curr_col_max += 1
                    curr_col_min += 1 # On cherche une colonne suivante
                else:
                    if curr_col_min == 0:
                        curr_col_min = -1 # On fait planter (bug)
                    else:
                        curr_col_min = 0

        y_min = min([b['y_min'] for b in bl_top])
        y_max = max([b['y_max'] for b in bl_top if b['y_min'] == y_min])

        ## y a-t-il plusieurs blocs alignés ? Si oui, on prend le plus à gauche.
        aligned_bl = [b for b in blocks if not b['treat'] and \
                b['temp_left'] >= curr_col_min and \
                b['temp_left'] < curr_col_max and \
                b['y_min'] >= y_min - VERTICAL_ALIGMENT_THRESHOLD and \
                b['y_min'] < y_max]
        selected_bl = aligned_bl[0]
        for b in aligned_bl[1:]:
            if b['x_min'] < selected_bl['x_min']: selected_bl = b

        if PRINT_SORT:
            print("{(%s, %s);(%s, %s)} %s" % (selected_bl['x_min'],
               selected_bl['x_max'], selected_bl['y_min'], selected_bl['y_max'],
               selected_bl['lines'][0]['text']))

        # On a le prochain bloc candidat : (selected_bl)
        # Cas de figure :
        # 1. (C) Il y a un bloc non traité qui commence +haut que le bas
        #    de selected_bl dans une colonne
        #    plus à gauche. C'est qu'on est passé un truc petit (centré?) qui
        #    nous a mis dans la mauvaise colonne. On va donc changer de colonne.
        # 2. (A) Le bloc trouvé ne chevauche pas la colonne suivante
        #    Alors on l'ajoute et RAS, mais on met les colonnes en cours
        #    à son col_min.
        # 3. (B) Le bloc trouvé chevauche la colonne suivante ou
        #    il y a un bloc non traité plus haut en FONT_HUGE. Alors on déplace
        #    son temp_left dans la colonne suivante et on cherche s'il
        #    n'y a pas des blocs plus hauts dans la colonne suivante.

        higher_bl = [b for b in blocks if not b['treat'] and \
            b['temp_left'] < curr_col_min and \
            b['y_min'] <= selected_bl['y_max']]

        if len(higher_bl) > 0: # ← Case 1
            if PRINT_SORT:
                print("→ Case C : %s" % selected_bl['lines'][0]['text'])

            curr_col_min = 0
            curr_col_max = 1
        elif selected_bl['col_max'] > curr_col_max :  # ← Case 3
            if PRINT_SORT:
                print("→ Case B : %s" % selected_bl['lines'][0]['text'])

            selected_bl['temp_left'] += 1
            curr_col_max += 1
            curr_col_min += 1
        else: # ← Case 2
            if PRINT_SORT:
                print("→ Case A : %s" % selected_bl['lines'][0]['text'])

            curr_col_max = selected_bl['col_max']
            curr_col_min = selected_bl['col_min']
            ordered_blocks.append(selected_bl)
            selected_bl['treat'] = True

    # - End loop : all blocks are in ordered_blocks.

    for b in blocks:
        if b['class'] == BL_IGNORE:
            ordered_blocks.append(b)

    for b in blocks:
        if b['class'] == BL_BOTTOM_PAGE:
            ordered_blocks.append(b)

    # Before leaving function
    for b in blocks:
        del b['treat']
        del b['temp_left']
    return ordered_blocks


# +--------------------------------------------------------------+
# |                    compute_similarity                        |
# +--------------------------------------------------------------+
# Effectue un calcul d'indice de similarité entre un bloc et un prototype.
# Le résultat est normalisé entre 0 (rien à voir) et 1.0 (exactement ça).
# Dans un premier temps, les coefs seront hardcodés dans cette fonction.
# On verra ensuite ceux qui sont importants à déplacer dans un config.py
def compute_similarity(block, prototype):
    # To ease code writing :
    bl = block
    pr = prototype
    
    if pr is None:
      return 0.0

    if pr['nb'] < 0.8: # Another wayt to say "if nb == 0" considering IEEE 754…
        return 0.0

    # Number of lines ; [<= 2 lines , 3 to 5 lines , >= 6 lines ]
    nl = 0.0
    if bl['nb_lines'] <= 2:
        nl = float(pr['nb_lines'][0]) / pr['nb_max']
    elif bl['nb_lines'] <= 5:
        nl = float(pr['nb_lines'][1]) / pr['nb_max']
    else:
        nl = float(pr['nb_lines'][2]) / pr['nb_max']

    # Block size
    bs = 1.0/max(1.0, abs(pr['block_size'] - float(bl['block_size'])))

    # Font
    ft = 0.0
    if pr['font'].get(bl['font']['id']) is not None:
        ft = 1.0

    # Font size : a way to avoid "not exactly same font" giving
    # same value as "completely different"
    # The result should be in [0.0 ; 4.0]
    fs = 4.0 / max(1.0, 2.0 * abs(pr['font_size'] - bl['font']['size']))
    # Captions should have quite precisely the same font size :
    if pr['class'] == BL_CAPTION:
      fs = 4.0 / max(1.0, 4.0 * abs(pr['font_size'] - bl['font']['size']))
    # Misc shouldn't be bigger than default font size
    if pr['class'] == BL_MISC and bl['font']['size'] > pr['font_size']:
      fs = 0.0

    # Alignment
    al = 0.0
    if bl['alignment'] != ALIGN_UNDEF and \
      pr['alignment'].get(bl['alignment']) is not None:
        al = float(pr['alignment'][bl['alignment']]) / pr['nb']

    # Max line size
    ml = 1.0 / max(1.0, abs(pr['max_line_size'] - bl['max_line_size']))

    # Final ponctuation
    po = float(pr['nb_punctations']) / pr['nb']
    m = re.search(PUNCTUATION_REGEX, bl['last_character'])
    if m is None:
        po = 1.0 - po

    ## fs is in [0.0 ; 4.0]. Other ones are in [0.0 ; 1.0].
    return (nl + bs + 6.0*ft + fs + al + ml + po) / 15.0


# +--------------------------------------------------------------+
# |                  get_closest_block_class                     |
# +--------------------------------------------------------------+
# Returns the class of the "most similar" block considering prototypes
def get_closest_block_class(block, BLOCK_PROTOTYPES):
    max_score = 0.0
    closest_index = -1

    for i in range(len(BLOCK_PROTOTYPES)):
        if BLOCK_PROTOTYPES[i] is not None and \
           BLOCK_PROTOTYPES[BL_PARAGRAPH] is not None:
            # Exclude comparison with titles if font_size smaller than default's
            exclude = block['font']['size'] < BLOCK_PROTOTYPES[BL_PARAGRAPH]['font_size'] \
                    and i >= BL_DOCUMENT_TITLE and i < BL_TITLE_5
            # A paragraphed font size can only be a title 5
            # or its font is exactly used for a title
            exclude |= block['font']['size'] == BLOCK_PROTOTYPES[BL_PARAGRAPH]['font_size'] \
                 and i == BL_TITLE_5 \
                 and BLOCK_PROTOTYPES[i]['font'].get(block['font']['id']) is None

            if not exclude:
                score = compute_similarity(block, BLOCK_PROTOTYPES[i])
                if score > max_score:
                    max_score = score
                    closest_index = i
    if closest_index < 0:
        return BL_UNDEF, 0.0

    return closest_index, max_score


# +--------------------------------------------------------------+
# |                  adjust_block_prototypes                     |
# +--------------------------------------------------------------+
# Le tableau de prototype contient des moyennes pour les différents
# attributs des blocs identifiés. Cette fonction ajuste la valeur
# de ces attributs avec celles d'un bloc nouvellement marqué.
def new_avg(cl, n, key, new_value, BLOCK_PROTOTYPES):
    BLOCK_PROTOTYPES[cl][key] = ((n * BLOCK_PROTOTYPES[cl][key]) \
        + float(new_value)) / (n + 1.0)

def adjust_block_prototypes(block, BLOCK_PROTOTYPES):
    # To ease code writing :
    bl = block
    cl = bl['class']
    proto = BLOCK_PROTOTYPES

    if proto[cl] is None:
        proto[cl] = {
            'class': cl,
            'nb': 1.0,
            'nb_max' : 1.0, # max nb value for all blocks.
            'nb_lines': [0,0,0],
            'block_size': float(bl['block_size']),
            'font_size': float(bl['font']['size']),
            'alignment': {},
            'font': {},
            'line_height': bl['line_height'],
            'max_line_size': float(bl['max_line_size']),
            'not_numbers': bl['not_numbers'],
            'nb_punctations': 0
        }
        proto[cl]['nb_max'] = max([p['nb_max'] for p in proto if p is not None])
    else:
        n = proto[cl]['nb']
        proto[cl]['nb'] += 1.0
        if proto[cl]['nb'] > proto[cl]['nb_max']:
          for p in proto:
            if p is not None:
              p['nb_max'] = proto[cl]['nb']
        new_avg(cl, n, 'block_size', block['block_size'], BLOCK_PROTOTYPES)
        new_avg(cl, n, 'font_size', block['font']['size'], BLOCK_PROTOTYPES)
        new_avg(cl, n, 'max_line_size', block['max_line_size'], BLOCK_PROTOTYPES)
        new_avg(cl, n, 'not_numbers', block['not_numbers'], BLOCK_PROTOTYPES)
        new_avg(cl, n, 'line_height', block['line_height'], BLOCK_PROTOTYPES)

    if proto[cl]['alignment'].get(bl['alignment']) is None:
        proto[cl]['alignment'][bl['alignment']] = 1
    else:
        proto[cl]['alignment'][bl['alignment']] += 1
    if proto[cl]['font'].get(bl['font']['id']) is None:
        proto[cl]['font'][bl['font']['id']] = 1
    else:
        proto[cl]['font'][bl['font']['id']] += 1
    m = re.search(PUNCTUATION_REGEX, block['last_character'])
    if m is not None:
        proto[cl]['nb_punctations'] += 1

    # We consider : 
    # ["number of blocks having less than 2 lines", "… between 3 and 5", "6 or more"]
    if bl['nb_lines'] <= 2:
        proto[cl]['nb_lines'][0] += 1
    elif bl['nb_lines'] <= 5:
        proto[cl]['nb_lines'][1] += 1
    else:
        proto[cl]['nb_lines'][2] += 1


# +--------------------------------------------------------------+
# |                  block_contains_keywords                     |
# +--------------------------------------------------------------+
def block_contains_keywords(block, list_of_regex):
    t = " ".join([t['text'] for t in block['lines']])
    for l in list_of_regex:
        if re.search(l, t, flags=re.IGNORECASE):
            return True
    return False


# +--------------------------------------------------------------+
# |                      guess_structure                         |
# +--------------------------------------------------------------+
def guess_structure(blocks, fontspec, tables):
    BLOCK_PROTOTYPES = [None for dummy in range(len(BLOCKS_CLASSES))]
    BLOCK_PROTOTYPES_2 = [None for dummy in range(len(BLOCKS_CLASSES))]

    # Count number of blocks for each font
    for f in fontspec:
        f['nb_blocks'] = len([b for b in blocks if b['font']['id'] == f['id']])

    ## First, identify the most used font from default font size
    ## and number of characters of each block.
    fonts = {}
    default_font = None
    default_font_nb_car = -1
    def_size = 0
    for f in fontspec:
      if f['nb_cars'] > default_font_nb_car:
        default_font_nb_car = f['nb_cars']
        default_font = f
        def_size = f['size']

    pages = {}
    if default_font is None:
      for bl in blocks:        
        if pages.get(bl['page']) is None:
          pages[bl['page']] = { 'blocks':[] }
        pages[bl['page']]['blocks'].append(bl)
      return pages
    
    default_font['is_default'] = True
    def_height = 0.0
    nb = 0
    for bl in blocks:
      if bl['font']['id'] == default_font['id']:
        def_height += bl['line_height']
        nb += 1
    if nb > 0:
      def_height /= float(nb)
    else:
      def_height = default_font['height'] # Not a good value since poppler 22-06.
      
    word_counter = re.compile(r'[a-zA-Z_À-ÿ]+', flags=re.U)
    blk_cel = 1
    blk_li = 1
    for bl in blocks:
        if pages.get(bl['page']) is None:
            pages[bl['page']] = { 'blocks':[] }

        # Score is given in similarity process. If score remains -1.0
        # after it, it means that the block have been identified
        # before.
        bl['score'] = -1.0

        if len(bl['lines']) == 1 and \
          len(re.sub(r'\W','', bl['lines'][0]['text']).strip()) == 0:
          bl['class'] = BL_IGNORE
          bl['nb_words'] = 0
        else:
          bl['class'] = BL_UNDEF
          
          # Is it a table ?
          ta = None
          for t in tables:
            if (t['page'] == bl['page']) \
              and (bl['pageheight']-t['y_max']-TABLE_LOC_THR_TOP <= bl['y_min']) \
              and (bl['pageheight']-t['y_min']+TABLE_LOC_THR_BTM >= bl['y_max']) \
              and (t['x_min']-TABLE_LOC_THR_LEFT <= bl['x_min']) \
              and (t['x_max']+TABLE_LOC_THR_RIGHT >= bl['x_max']):
              ta = t
              break
          if ta is not None: # The block is part of a table
            if ta.get('has_block') is None: # Replace current block with table
              ta['has_block'] = True
              bl['class'] = BL_TABLE
              bl['y_min'] = bl['pageheight']-ta['y_max']
              bl['y_max'] = bl['pageheight']-ta['y_min']
              bl['x_min'] = ta['x_min']
              bl['x_max'] = ta['x_max']
              bl['lines'] = []
              bl['nb_words'] = 0
              for lines in ta['cells']:
                if len(lines) > 0:
                  li = { 'flags': FLAG_NONE,  'words': [],
                    'text': '<tr num="%d">\n' % blk_li,
                    'height': lines[0].y2-lines[0].y1, 'font':bl['font'],
                    'x_min': lines[0].x1, 'x_max': lines[-1].x2,
                    'y_min': lines[0].y1, 'y_max': lines[0].y2}
                  blk_li += 1
                if len(lines) == 1:
                  cel = lines[0]
                  for ttxt in TABLE_SEPARATORS_REGEX.split(cel.text):
                    bl['nb_words'] += len(word_counter.findall(ttxt))
                    li['text'] += '<td num="%d">%s</td>\n' % (blk_cel, ttxt)
                    blk_cel += 1
                    li['words'].append({'height': cel.y2-cel.y1, 'text': ttxt})
                elif len(lines) > 0:
                  for cel in lines:
                    ttxt = cel.text.strip()
                    bl['nb_words'] += len(word_counter.findall(ttxt))
                    li['text'] += '<td num="%d">%s</td>\n' % (blk_cel, ttxt)
                    blk_cel += 1
                    li['words'].append({'height': cel.y2-cel.y1, 'text': cel.text.strip()})
                if len(lines) > 0:
                  li['text'] += '</tr>'
                  bl['lines'].append(li)
              pages.get(bl['page'])['blocks'].append(bl)
            else: # mark block to be removed.
              bl['class'] = BL_TABLE
              bl['nb_words'] = 0
              bl['lines'] = [] # If length is 0, won't be displayed
          # It's not a table :
          if bl['class'] == BL_UNDEF:
            bl['nb_words'] = sum([len(word_counter.findall(l['text'])) for l in bl['lines']])
            pages.get(bl['page'])['blocks'].append(bl)


        nb_car = max_car = 0
        min_just = min_right = min_center = min_left = bl['x_max']
        max_right = max_center = max_left = bl['x_min']
        last_character = ''
        non_numbers = 0
        has_bullets = False
        for l in bl['lines']:
            nb_car += len(l['text'])
            non_numbers += len(re.sub(NUMBERING_REGEX,'', l['text']))
            max_car = max(max_car, len(l['text']))
            if l == bl['lines'][-1]: min_just = min_right
            if l['x_min'] < min_left: min_left = l['x_min']
            if l['x_min'] > max_left: max_left = l['x_min']
            if l['x_max'] < min_right: min_right = l['x_max']
            if l['x_max'] > max_right: max_right = l['x_max']
            c = (l['x_min'] + l['x_max']) / 2
            if c < min_center: min_center = c
            if c > max_center: max_center = c
            if len(l['text']) > 0:
                last_character = l['text'].strip()[-1]
            else:
                last_character = '.'
            if len(l['text'].split(':')) > 1 and \
                    len(re.sub(r'[A-ZÀÇÉÈÊÂÔÛ0-9]','', l['text'][0])) == 0:
                l['flags'] |= IS_DESCRIPTION
            if len(re.sub(r'[]\W\(\[\{]','', l['text'][0:1]).strip()) == 0:
                l['flags'] |= HAS_BULLET
                has_bullets = True
            # - End loop on lines

        # All lines have been seen. Conclusions.
        if has_bullets:
            bl['flags'] |= HAS_BULLET
        bl['block_size'] = nb_car
        bl['max_line_size'] = max_car
        if nb_car > 0:
            bl['not_numbers'] = float(non_numbers) / float(nb_car)
        else:
            bl['not_numbers'] = 0.0 # Should not pass (here)
        bl['min_just'] = min_just     # Min value of xMax except for last line
        bl['min_left'] = min_left     # Min value of xMin
        bl['max_left'] = max_left     # Max value of xMin
        bl['min_right'] = min_right   # Min value of xMax
        bl['max_right'] = max_right   # Max value of xMin
        bl['min_center'] = min_center # Min value of (xMax + xMin) / 2
        bl['max_center'] = max_center # Max value of (xMax + xMin) / 2
        bl['last_character'] = last_character
        bl['nb_lines'] = len(bl['lines'])

        ## Font_class
        if USE_FONTSIZE:
          size_value = bl['font']['size']
          base_value = def_size
        else:
          size_value = bl['line_height']
          base_value = def_height

        if size_value <= base_value + FONT_THRESHOLDS[FONT_TINY]:
            bl['font_class'] = FONT_TINY
        elif size_value <= base_value + FONT_THRESHOLDS[FONT_SMALL]:
            bl['font_class'] = FONT_SMALL
        elif size_value >= base_value + FONT_THRESHOLDS[FONT_HUGE]:
            bl['font_class'] = FONT_HUGE
        elif size_value >= base_value + FONT_THRESHOLDS[FONT_BIG]:
            bl['font_class'] = FONT_BIG
        else:
            bl['font_class'] = FONT_DEFAULT
        bl['font_size'] = size_value

        # On en fait une deuxième basée sur la hauteur de ligne,
        # car la reconnaissance des lignes verticales ne fonctionne pas.
        # Du coup on ne le fait que pour les verticales.
        if ((bl['flags'] & FLAG_VERTICAL) != 0) and (len(bl['lines']) > 0):
          bl['font_size'] = sum([li['height'] for li in bl['lines']]) / float(len(bl['lines']))
          if bl['font_size'] <= base_value + FONT_THRESHOLDS[FONT_TINY]:
              bl['font_class'] = FONT_TINY
          elif bl['font_size'] <= base_value + FONT_THRESHOLDS[FONT_SMALL]:
              bl['font_class'] = FONT_SMALL
          elif bl['font_size'] >= base_value + FONT_THRESHOLDS[FONT_HUGE]:
              bl['font_class'] = FONT_HUGE
          elif bl['font_size'] >= base_value + FONT_THRESHOLDS[FONT_BIG]:
              bl['font_class'] = FONT_BIG
          else:
              bl['font_class'] = FONT_DEFAULT
        # Fin

        if bl['font']['family'] is None: # style : bold, italic, ...
            bl['font']['has_style'] = False
        else:
            bl['font']['has_style'] = (len(bl['font']['family'].split(',')) > 1)

        # Alignment
        if len(bl['lines']) <= 1:
            bl['alignment'] = ALIGN_UNDEF
        elif len(bl['lines']) > 2 and \
            (max_right - min_just) < VERTICAL_ALIGMENT_THRESHOLD and \
            (max_left - min_left) < VERTICAL_ALIGMENT_THRESHOLD:
            bl['alignment'] = ALIGN_JUSTIFIED
        elif (max_left - min_left) < VERTICAL_ALIGMENT_THRESHOLD:
            bl['alignment'] = ALIGN_LEFT
        elif (max_right - min_right) < VERTICAL_ALIGMENT_THRESHOLD:
            bl['alignment'] = ALIGN_RIGHT
        elif (max_center - min_center) < VERTICAL_ALIGMENT_THRESHOLD:
            bl['alignment'] = ALIGN_CENTERED
        else:
            bl['alignment'] = ALIGN_UNDEF

        # - End loop on blocks


    # Page top and bottom detection is better before block sorting because of
    # column detection consequences...
    mark_page_top(pages)
    mark_page_bottom(pages)

    # Reorder blocks
    first_block_tagged = False
    for p in pages.values():
      if len(p['blocks']) > 0:
        compute_lrud(p['blocks'])
        # Let's keep an expansion based on font_size for now…
        expand_blocks(p['blocks'], default_font['size'])
        col = get_columns(p['blocks'], default_font['size'])
        col.append(max(b['x_max'] for b in p['blocks']))
        p['blocks'] = sort_blocks(p['blocks'], col, default_font['size'])
        
        if not first_block_tagged:
          try:
            bl = next(b for b in p['blocks'] if b['class'] != BL_TOP_PAGE)
            bl['flags'] |= FLAG_FIRST_BLOCK
            first_block_tagged = True
          except :
            pass
    # - End loop on pages

    # ...but sometimes it works better after blocks sorting
    mark_page_top(pages)
    mark_page_bottom(pages)

    # ===== There, we start blocks identification =====

    # It consists in searching for criterias defining blocks
    # quite surely (with no doubt).

    # Vertical text is considered misc until its font is identificated well
    for p in pages.values():
      for bl in p['blocks']:
        if (bl['flags'] & FLAG_VERTICAL != 0) and (bl['class'] != BL_TABLE):
            bl['class'] = BL_MISC
            # Prototype of BL_MISC is not adjusted because vertical text
            # is too different and should need different treatments.

    # Ignore blocks containing no character or number
    for p in pages.values():
      for bl in p['blocks']:
        if bl['class'] == BL_UNDEF:
            t = "".join([t['text'] for t in bl['lines']])
            if len(re.sub(r'\W','',t).strip()) == 0:
                bl['class'] = BL_IGNORE

    # Credits and/or copyright, or contact (e-mail) based on keywords
    for bl in blocks:
        if bl['class'] == BL_UNDEF and bl['font'] is not default_font:
            if block_contains_keywords(bl, CREDITS_REGEX):
                bl['class'] = BL_MISC # BL_CREDITS
            elif block_contains_keywords(bl, COPYRIGHT_REGEX):
                bl['class'] = BL_MISC # BL_COPYRIGHT
            elif block_contains_keywords(bl, CONTACT_REGEX):
                bl['class'] = BL_MISC # BL_CONTACT
            if  bl['class'] != BL_UNDEF:
                adjust_block_prototypes(bl, BLOCK_PROTOTYPES)
                adjust_block_prototypes(bl, BLOCK_PROTOTYPES_2)

    # Paragraphs
    ## default_font AND max_line_size >= PARAGRAH_LINE_SIZE
    ## AND last_character is ponctuation OR HAS_BULLET
    ## AND is not centered nor right_aligned.
    for bl in blocks:
        if bl['class'] == BL_UNDEF and bl['font'] is default_font and \
            (re.search(PUNCTUATION_REGEX, bl['last_character']) is not None \
            or (bl['flags'] & HAS_BULLET != 0)) and \
            bl['max_line_size'] >= PARAGRAH_LINE_SIZE and \
            bl['alignment'] != ALIGN_CENTERED and \
            bl['alignment'] != ALIGN_RIGHT :
                bl['class'] = BL_PARAGRAPH
                adjust_block_prototypes(bl, BLOCK_PROTOTYPES)
                adjust_block_prototypes(bl, BLOCK_PROTOTYPES_2)
                
    # Misc
    ## Long lines having text size smaller then default's are BL_MISC.
    for bl in blocks:
      if bl['class'] == BL_UNDEF and bl['font_class'] < FONT_DEFAULT and \
          re.search(PUNCTUATION_REGEX, bl['last_character']) is not None \
          and bl['max_line_size'] >= PARAGRAH_LINE_SIZE :
        bl['class'] = BL_MISC
        adjust_block_prototypes(bl, BLOCK_PROTOTYPES)
        adjust_block_prototypes(bl, BLOCK_PROTOTYPES_2)

    # Captions
    ## We'll look for keywords like "Crédit photo".
    ## If we don't find any, we'll try small fonts
    ## centered or aligned left and quite short (less than PARAGRAH_LINE_SIZE)
    nb_caption = 0
    for bl in blocks:
        if bl['class'] == BL_UNDEF and bl['font_class'] <= FONT_DEFAULT and \
                block_contains_keywords(bl, CAPTION_REGEX):
            bl['class'] = BL_CAPTION
            adjust_block_prototypes(bl, BLOCK_PROTOTYPES)
            adjust_block_prototypes(bl, BLOCK_PROTOTYPES_2)
            nb_caption += 1

    if nb_caption == 0: # Let's try something else
      for bl in blocks:
        bltxt = " ".join([t['text'] for t in bl['lines']]).strip()
        if bl['class'] == BL_UNDEF and bl['font_class'] < FONT_DEFAULT and \
                bl['max_line_size'] < SMALL_LINES_LIMIT and \
                (bl['alignment'] == ALIGN_CENTERED or \
                  bl['alignment'] == ALIGN_LEFT or \
                  bl['alignment'] == ALIGN_UNDEF):
          bl['class'] = BL_CAPTION
          adjust_block_prototypes(bl, BLOCK_PROTOTYPES)
          adjust_block_prototypes(bl, BLOCK_PROTOTYPES_2)
          nb_caption += 1

    # Links (or "See also")
    ## Just look for http(s)://
    ## Won't use it as a style. We just avoid it being a title.
    for bl in blocks:
     if bl['class'] == BL_UNDEF and block_contains_keywords(bl, LINK_REGEX) \
            and bl['font_class'] >= FONT_DEFAULT:
         bl['class'] = BL_PARAGRAPH # BL_LINK # Shouldn't be BL_MISC ?

    # Titles
    title_block_fonts = []
    title_nb = []
    title_blocks = []
    title_nb_other = [0]
    last_block_font = -2
    for p in pages.values():
        for b in p['blocks']:
            if b['class'] == BL_UNDEF and b['block_size'] <= TITLE_SIZE_MAX and \
              re.search(PUNCTUATION_REGEX, b['last_character']) is None \
              and (b['line_height'] > def_height \
                  or (b['font_class'] == FONT_DEFAULT and b['font']['has_style'])):
                if b['font']['id'] == last_block_font:
                    title_nb[-1] += 1 #len(b['lines'])
                    title_blocks[-1].append(b)
                else:
                    title_block_fonts.append(b['font'])
                    title_nb.append(1)
                    title_nb_other.append(0)
                    title_blocks.append([b])
                last_block_font = b['font']['id']
            else:
                title_nb_other[-1] += 1

    ## We remove elements having too many successive lines
    del title_nb_other[0] # The 1st element has no meaning
    i = 0
    while i < len(title_nb):
        if title_nb[i] > TITLE_MAX_LINES and title_nb_other[i] == 0:
            del title_block_fonts[i]
            del title_blocks[i]
            del title_nb[i]
            del title_nb_other[i]
        else:
            i += 1

    ## document title
    if len(title_block_fonts) > 0 and \
            title_block_fonts[0]['nb_blocks'] == title_nb[0]:
        for b in blocks:
            if b['class'] == BL_UNDEF and b['font'] == title_block_fonts[0]:
                b['class'] = BL_DOCUMENT_TITLE
                title_nb[0] -= 1
                if (title_nb[0] == 0):
                    del(title_nb[0])
                    del(title_block_fonts[0])
                    del(title_blocks[0])
                    break

    # On essaye des niveaux de titres juste en fonction de leur taille ?
    # RQ : Ça devrait pas suffire, il faudra aussi les successions et attributs
    # On ajoute les attributs, pour voir.
    title_font_sizes = []
    for f in title_block_fonts:
        sz = f['size']
        if f['has_style']: 
          sz += 0.5
        sz += 0.45 - min(0.45, 0.1 * float(round(f['nb_blocks'] / 3)))
        if title_font_sizes.count(sz) == 0:
            title_font_sizes.append(sz)
    title_font_sizes.sort(reverse=True)

    # On fait un tableau tel que profondeur(blks[i])=title_depth[i]
    title_depth = []
    for blks in title_blocks:
        b = blks[0]
        sz = b['font']['size']
        if b['font']['has_style']: sz += 0.5
        sz += 0.45 - min(0.45, 0.1 * float(round(b['font']['nb_blocks'] / 3)))
        title_depth.append(title_font_sizes.index(sz))

    # Et maintenant on essaye d'optimiser ce tableau pour remonter
    # le niveau des titres.
    TITLE_OPTIMISE_LEVEL = True
    if (TITLE_OPTIMISE_LEVEL):
        preced = [[] for _ in range(len(title_font_sizes))]
        if (len(title_depth) > 0):
            last_depth = title_depth[0]
            for d,p in zip(title_depth[1:], title_depth[:-1]):
                if d > p and not p in preced[d]:
                    preced[d].append(p)
            title_font_depth = []
            
            if USE_FONTSIZE:
              thresh = def_size + FONT_THRESHOLDS[FONT_HUGE]
            else:
              thresh = def_height + FONT_THRESHOLDS[FONT_HUGE]
            for p,s in zip(preced,title_font_sizes):
                if len(p) > 0:
                    title_font_depth.append(max([title_font_depth[d] for d in p])+1)
                else:
                    if s >= thresh or len(title_font_depth) == 0:
                        title_font_depth.append(0)
                    else:
                        title_font_depth.append(1)
            for i in range(len(title_depth)):
                title_depth[i] = min(title_font_depth[title_depth[i]],
                                     len(TITLE_CLASSES)-1)

    title_fonts = [] # To be used at the begining of prototype distance process
    for blks,depth in zip(title_blocks, title_depth):
        for b in blks:
            if b['font'] not in title_fonts:
                title_fonts.append(b['font'])
            b['class'] = TITLE_CLASSES[depth]
            adjust_block_prototypes(b, BLOCK_PROTOTYPES)
            adjust_block_prototypes(b, BLOCK_PROTOTYPES_2)

    # - Now, we complete undefined blocks with closest prototypes.
    
    # 1st pass ; we don't mark titles (because we'll do
    # a special treatment for multi-line titles after
    # the second phase).
    for p in pages.values():
      for b in p['blocks']:
        if b['class'] == BL_UNDEF:
          proto,score = get_closest_block_class(b, BLOCK_PROTOTYPES)
           
          if proto in TITLE_CLASSES:
            if (b['block_size'] > TITLE_SIZE_LIMIT or \
                len(b['lines']) >= TITLE_MAX_LINES):
              score = compute_similarity(b, BLOCK_PROTOTYPES[BL_PARAGRAPH])
              proto = BL_PARAGRAPH

          if score > FIRST_PASS_THR and proto not in TITLE_CLASSES:
            b['class'] = proto
            b['score'] = score
            adjust_block_prototypes(b, BLOCK_PROTOTYPES_2)


    # --- 2nd pass
    for p in pages.values():
      for b in p['blocks']:
        if b['class'] == BL_UNDEF:
          proto,score = get_closest_block_class(b, BLOCK_PROTOTYPES_2)
          b['class'] = proto
          b['score'] = score
    
    # Titles : Too many successive title lines are marked paragraph.
    def replace_title_w_paragraph(list_blocks):
      if len(list_blocks) <= TITLE_MAX_LINES:
        return list_blocks
      for bbl in list_blocks:
        bbl['class'] = BL_PARAGRAPH
        bbl['score'] = compute_similarity(bbl, BLOCK_PROTOTYPES_2[BL_PARAGRAPH])
        adjust_block_prototypes(bbl, BLOCK_PROTOTYPES_2)
      return []

    title_stack = []
    prev_class = BL_UNDEF
    for p in pages.values():
      for b in p['blocks']:
        is_title = (b['class'] in TITLE_CLASSES)
        if is_title:
          if b['class'] != prev_class:
            replace_title_w_paragraph(title_stack)
            title_stack = []
          title_stack.append(b)
        else:
          replace_title_w_paragraph(title_stack)
          title_stack = []
        prev_class = b['class']
    replace_title_w_paragraph(title_stack)

    return pages

# +--------------------------------------------------------------+
# |                    text_transformations                      |
# +--------------------------------------------------------------+
def text_tr(text):
    if APOSTROPHE_REGEX:
        text = re.sub(APOSTROPHE_REGEX, "'", text)
    if DOUBLEQUOTE_REGEX:
        text = re.sub(DOUBLEQUOTE_REGEX, '"', text)
    if TO_BE_REMOVED:
        text = re.sub(TO_BE_REMOVED, ' ', text)
    text = re.sub(r' +', ' ', text)
    return text.strip()

# +--------------------------------------------------------------+
# |                         print_html                           |
# +--------------------------------------------------------------+
def print_html(pages, fontspec, out=sys.stdout):
    nb_words = 0
    blocks = []
    for p in pages.values():
        for b in p['blocks']:
            blocks.append(b)

    print('<!DOCTYPE html>', file=out)
    print('<html lang="fr">', file=out)
    print('<head><meta charset="utf-8" />', file=out)
    if PRINT_CSS:
        print('<link rel="stylesheet" href="%s" />' % URL_CSS, file=out)
        print('<link rel="stylesheet" href="bsv.css" />', file=out)

    if DEBUG_PRINT:
        print("<!-- Fonts :", file=out)
        for f in fontspec:
            if f['nb_blocks'] > 0:
                if f.get('is_default') is None:
                    df = ' '
                else:
                    df = '*'
                print(" %s[%3d] { size:%3d, nb_blocks: %d, nb_cars:%d, color:%s, family:%s }" %(
                    df, f['id'], f['size'], f['nb_blocks'], f['nb_cars'], f['color'], f['family'],
                    ), file=out)
        print("-->", file=out)

    for b in blocks:
      if b.get('class') is None:
        b['class'] = BL_UNDEF
    doc_title = ''
    if len(blocks) > 0:
      doc_title = [b for b in blocks if b['class'] == BL_DOCUMENT_TITLE]
    if len(doc_title) > 0:
        txt = '  <title>'
        for b in doc_title:
            for l in b['lines']:
                txt = '%s%s ' % (txt, l['text'])
        if txt[-1] == ' ':
            txt = txt[:-1]
        print("%s</title>" % text_tr(txt), file=out)
    else:
      print('  <title></title>', file=out)
    print("</head>", file=out)
    print("<body>", file=out)

    BLOCK_TAGS = ['div', 'p', 'footer', 'header', 'figcaption', 'table',
        'a', 'h1', 'h1', 'h2', 'h3', 'h4', 'h5',
        'div', 'div', 'div', 'small', '']
    BLOCK_ENDLINES = ['<br />', '', '<br />', '<br />', '', '',
        '', '', '', '', '', '', '', '<br />', '<br />', '<br />', '',
        '']
    BLOCK_NUM = {}
    for tta in BLOCK_TAGS:
      if BLOCK_NUM.get(tta) is None:
        BLOCK_NUM[tta] = 1
    NUM_FIGURE = 1

    i = 0
    last_page = 1
    ORDER = 1
    while i < len(blocks):
        if blocks[i]['page'] != last_page:
            print("<hr /><!-- =============== Page %d =============== -->" % blocks[i]['page'], file=out)
            last_page = blocks[i]['page']
        cl = blocks[i]['class']
        if cl == BL_TABLE:
          if len(blocks[i]['lines']) > 0:
            nb_words += blocks[i]['nb_words']
            if not DEBUG_PRINT:
                print('<table border="1" num="%d" order="%d">' % (
                    BLOCK_NUM[BLOCK_TAGS[cl]], ORDER), file=out)
            elif blocks[i]['score'] is None:
                print('<table border="1" font="%d" num="%d" order="%d">' % (
                        blocks[i]['font']['id'], BLOCK_NUM[BLOCK_TAGS[cl]],
                        ORDER), file=out)
            else:
                print('<table border="1" font="%d" score="%f" num="%d" order="%d">' % (
                        blocks[i]['font']['id'], blocks[i]['score'], 
                        BLOCK_NUM[BLOCK_TAGS[cl]], ORDER), file=out)
            for li in blocks[i]['lines']:
              print(li['text'], file=out)
            print('</table>', file=out)
            BLOCK_NUM[BLOCK_TAGS[cl]] += 1
            ORDER += 1

        elif cl != BL_IGNORE:
            txt = pre = post = ''
            if blocks[i].get('nb_words') is not None:
              nb_words += blocks[i]['nb_words']
            if PRINT_CSS:
                id_cl = ' class="%s"' % BLOCKS_CLASSES[cl]
            else:
                id_cl = ''
            if cl == BL_CAPTION:
                pre = '<figure num="%d" order="%d">' % (NUM_FIGURE, ORDER)
                NUM_FIGURE += 1
                #ORDER += 1
                post = '</figure>'
            if cl == BL_MISC:
              post = '</p>'
              if not DEBUG_PRINT:
                txt = '%s<p num="%d" order="%d"><%s%s num="%d" order="%d">' % (
                      txt, BLOCK_NUM[BLOCK_TAGS[BL_PARAGRAPH]], ORDER,
                      BLOCK_TAGS[cl], id_cl, BLOCK_NUM[BLOCK_TAGS[cl]],
                      ORDER)
              elif blocks[i]['score'] is None:
                txt = '%s<p font="%d" num="%d" order="%d"><%s%s num="%d" order="%d">' % (
                      txt, blocks[i]['font']['id'],
                      BLOCK_NUM[BLOCK_TAGS[BL_PARAGRAPH]], ORDER,
                      BLOCK_TAGS[cl], id_cl, BLOCK_NUM[BLOCK_TAGS[cl]],
                      ORDER)
              else:
                txt = '%s<p font="%d" score="%f" num="%d" order="%d"><%s%s num="%d" order="%d">' % (
                      txt, blocks[i]['font']['id'], blocks[i]['score'], 
                      BLOCK_NUM[BLOCK_TAGS[BL_PARAGRAPH]], ORDER,
                      BLOCK_TAGS[cl], id_cl, BLOCK_NUM[BLOCK_TAGS[cl]], 
                      ORDER)
              BLOCK_NUM[BLOCK_TAGS[BL_PARAGRAPH]] += 1
              BLOCK_NUM[BLOCK_TAGS[cl]] += 1
              ORDER += 1
            else: 
              if not DEBUG_PRINT:
                txt = '%s%s<%s%s num="%d" order="%d">' % (txt, pre,
                      BLOCK_TAGS[cl], id_cl, BLOCK_NUM[BLOCK_TAGS[cl]],
                      ORDER)
              elif blocks[i]['score'] is None:
                txt = '%s%s<%s%s font="%d" num="%d" order="%d">' % (txt,
                      pre, BLOCK_TAGS[cl], id_cl, blocks[i]['font']['id'], 
                      BLOCK_NUM[BLOCK_TAGS[cl]], ORDER)
              else:
                txt = '%s%s<%s%s font="%d" score="%f" num="%d" order="%d">' % (
                    txt, pre, BLOCK_TAGS[cl], id_cl, blocks[i]['font']['id'],
                    blocks[i]['score'], BLOCK_NUM[BLOCK_TAGS[cl]], ORDER)
              BLOCK_NUM[BLOCK_TAGS[cl]] += 1
              ORDER += 1

            for l in blocks[i]['lines']:
                if (l['flags'] & (HAS_BULLET | IS_DESCRIPTION)) != 0 and \
                        BLOCK_ENDLINES[cl] == '' and \
                        l != blocks[i]['lines'][0]:
                    if txt.strip()[-1] == '-' and len(l['text']) > 0 and \
                                          l['text'][0] != ' ': # Césure
                        word_to_test = ("%s%s" % (txt.strip()[:-1].split(" ")[-1],
                            l['text'])).split(" ")[0]
                        word_to_test = re.sub(r'[^a-zA-Z\'’àâäéèêëïîôöùûüÀÂÄÉÈÊËÏÎÔÖÙÛÜ]', '', word_to_test)
                        if len(word_to_test) > 0:
                            if DICT.check(word_to_test):
                                txt = "%s%s " % (txt.strip()[:-1], l['text'])
                            else:
                                txt = "%s<br />%s " % (txt.strip(), l['text'])
                        else:
                            txt = "%s<br />%s " % (txt.strip(), l['text'])
                    else:
                        txt = "%s<br />%s " % (txt.strip(), l['text'])
                else:
                    if txt.strip()[-1] == '-' and l['text'][0] != ' ': # Césure
                        word_to_test = ("%s%s" % (txt.strip()[:-1].split(" ")[-1],
                            l['text'])).split(" ")[0]
                        word_to_test = re.sub(r'[^a-zA-Z\'’àâäéèêëïîôöùûüÀÂÄÉÈÊËÏÎÔÖÙÛÜ]', '', word_to_test)
                        if len(word_to_test) > 0:
                            if DICT.check(word_to_test):
                                txt = "%s%s " % (txt.strip()[:-1], l['text'])
                            else:
                                txt = "%s%s " % (txt.strip(), l['text'])
                        else:
                            txt = "%s%s " % (txt, l['text'])
                    else:
                        txt = "%s%s " % (txt, l['text'])
                    if BLOCK_ENDLINES[cl] != '':
                        txt = "%s%s " % (txt.strip(),BLOCK_ENDLINES[cl])
                    if l == blocks[i]['lines'][-1]:
                        txt = txt.strip()
            if txt[-1] == ' ':
                txt = txt[:-1]
            print("%s</%s>%s" % (text_tr(txt), BLOCK_TAGS[cl], post), file=out)
        else: # cl == BL_IGNORE:
            txt = '<div class=\"unknown\"  order="%d">' % ORDER
            ORDER += 1
            txt_len = 0
            for l in blocks[i]['lines']:
              txt_len += len(l['text'].strip())
              txt = "%s%s " % (txt, l['text'])
            if txt_len > 0:
              print("%s</div>" % text_tr(txt), file=out)

        i += 1

    if OUTPUT_LICENCE is not None:
      print(OUTPUT_LICENCE, file=out)
    print("</body><!-- %d words -->" % nb_words, file=out)
    print("</html>", file=out)
    return nb_words

# ---------------------------------------------------
#            returns the result in a string
# ---------------------------------------------------
def get_pdf2html(filename):
    blocks = get_pdftotext(filename)
    segments,fontspec = get_pdftohtml(filename)
    page_list = []
    for bl in blocks:
      if bl['page'] not in page_list:
        page_list.append(bl['page'])
    if (segments is None) or (len([page_list]) == 0):
      return ' ', 0
    tableaux = get_tables(filename, page_list)

    #default_font_size = get_default_font_size(fontspec)
    guess_fonts(blocks, segments, fontspec)
    
    pages = guess_structure(blocks, fontspec, tableaux)
    s = StringIO()
    nb_words = print_html(pages, fontspec, s)
    return str(s.getvalue()), nb_words
