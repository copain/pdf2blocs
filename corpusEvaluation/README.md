# Evaluation

les sorties de pdf2blocs ont été comparées aux sorties d'un logiciel du commerce Abbyy payant. 3 fichiers pdf ont été utilisé pour faire cette comparaison:
* un BSV en viticulture de la région Lorraine datant de 19 juin 2019: Ce fichier contient beaucoup d'images et du texte caché.
* un BSV en viticulture de la région Alsace datant du 26 juillet 2016: Ce fichier est une feuille recto verso imprimée en paysage. Il contient 4 colonnes avec des titres non homogènes. Ces bas de pages pausent aussi problème.
* un BSV vigne de la région Aquitaine datant du 9 avril 2019: la première page de ce fichier est en double colonnes. Il contient une grande typologie de titres et du texte dans des encadrés.
Ces fichiers pdf sont dans le répertoire pdf.

Le répertoire abbyy contient les fichiers html obtenus par le logiciel Abbyy.

Le répertoire pdf2blocs contient les fichiers html obtenus par le logiciel pdf2blocs.

Le répertoire goldstandard contient les sorties html espérées de la conversion pdf vers html.



Chaque erreur a été identifiée. La liste des erreurs est disponibles dans le fichier csv. 
Une version de ce fichier est disponible en ligne dans le google drive du projet d2kab / T4.3 / examplesBSV
https://docs.google.com/spreadsheets/d/1azBvHRj0aad6NbF3qvsKbY-aModLqNwztfE3J5dkvMQ/edit#gid=0
